﻿namespace Proyecto_MAD
{
    partial class Form8
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.moddata = new System.Windows.Forms.Button();
            this.moddom = new System.Windows.Forms.Button();
            this.modban = new System.Windows.Forms.Button();
            this.modemp = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.idem = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // moddata
            // 
            this.moddata.Location = new System.Drawing.Point(115, 38);
            this.moddata.Margin = new System.Windows.Forms.Padding(4);
            this.moddata.Name = "moddata";
            this.moddata.Size = new System.Drawing.Size(89, 71);
            this.moddata.TabIndex = 11;
            this.moddata.Text = "Modificar Datos Generales";
            this.moddata.UseVisualStyleBackColor = true;
            this.moddata.Click += new System.EventHandler(this.Add_Click);
            // 
            // moddom
            // 
            this.moddom.Location = new System.Drawing.Point(115, 136);
            this.moddom.Margin = new System.Windows.Forms.Padding(4);
            this.moddom.Name = "moddom";
            this.moddom.Size = new System.Drawing.Size(89, 71);
            this.moddom.TabIndex = 12;
            this.moddom.Text = "Modificar Domicilio";
            this.moddom.UseVisualStyleBackColor = true;
            this.moddom.Click += new System.EventHandler(this.moddom_Click);
            // 
            // modban
            // 
            this.modban.Location = new System.Drawing.Point(115, 242);
            this.modban.Margin = new System.Windows.Forms.Padding(4);
            this.modban.Name = "modban";
            this.modban.Size = new System.Drawing.Size(89, 71);
            this.modban.TabIndex = 13;
            this.modban.Text = "Modificar Banco";
            this.modban.UseVisualStyleBackColor = true;
            this.modban.Click += new System.EventHandler(this.Delete_Click);
            // 
            // modemp
            // 
            this.modemp.Location = new System.Drawing.Point(115, 340);
            this.modemp.Margin = new System.Windows.Forms.Padding(4);
            this.modemp.Name = "modemp";
            this.modemp.Size = new System.Drawing.Size(89, 71);
            this.modemp.TabIndex = 14;
            this.modemp.Text = "Modificar Empleado";
            this.modemp.UseVisualStyleBackColor = true;
            this.modemp.Click += new System.EventHandler(this.modemp_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(115, 448);
            this.back.Margin = new System.Windows.Forms.Padding(4);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(89, 71);
            this.back.TabIndex = 15;
            this.back.Text = "Volver";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(138, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 17);
            this.label15.TabIndex = 48;
            this.label15.Text = "Id Empleado";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // idem
            // 
            this.idem.AutoSize = true;
            this.idem.Location = new System.Drawing.Point(230, 9);
            this.idem.Name = "idem";
            this.idem.Size = new System.Drawing.Size(86, 17);
            this.idem.TabIndex = 49;
            this.idem.Text = "Id Empleado";
            this.idem.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 565);
            this.Controls.Add(this.idem);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.back);
            this.Controls.Add(this.modemp);
            this.Controls.Add(this.modban);
            this.Controls.Add(this.moddom);
            this.Controls.Add(this.moddata);
            this.Name = "Form8";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modificar Empleados";
            this.Load += new System.EventHandler(this.Form8_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button moddata;
        private System.Windows.Forms.Button moddom;
        private System.Windows.Forms.Button modban;
        private System.Windows.Forms.Button modemp;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label idem;
    }
}