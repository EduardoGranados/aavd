﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
	public partial class Form3 : Form
	{
		public Form3()
		{
			InitializeComponent();
		}

        private void Form3_Load(object sender, EventArgs e)
        {
            fecNac.MaxDate = DateTime.Today;
            fecNac.Format = DateTimePickerFormat.Custom;
            fecNac.CustomFormat = "yyyy-MM-dd";

            fecHir.MaxDate = DateTime.Today;
            fecHir.Format = DateTimePickerFormat.Custom;
            fecHir.CustomFormat = "yyyy-MM-dd";

            DateDpto.MaxDate = DateTime.Today;
            DateDpto.Format = DateTimePickerFormat.Custom;
            DateDpto.CustomFormat = "yyyy-MM-dd";

            Datepuesto.MaxDate = DateTime.Today;
            Datepuesto.Format = DateTimePickerFormat.Custom;
            Datepuesto.CustomFormat = "yyyy-MM-dd";



            EnlaceDB con = new EnlaceDB();

            var tabla = con.get_Deptos("X", aux.id_empresa);
            dgvDpto.DataSource = tabla;

            var tabla3 = con.get_Puesto("X", aux.id_empresa);
            dgvPuesto.DataSource = tabla3;

            var tabla2 = con.get_Empleado("X",aux.id_empresa);
            dgvEmpleado.DataSource = tabla2;

            Empresa.Text = aux.id_empresa.ToString();

            try
            {
                for (int i = 12; i < 46; i++)
                {
                    dgvEmpleado.Columns[i].Visible = false;
                }

            }
            catch (InvalidCastException) { }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Agregar
            string nombre=null, curp = null, nss = null, rfc = null, email = null, pass = null, calle = null, municipio = null,
                estado = null, banco = null, fec_pue = null, fec_dpto = null, fec_hired = null, fec_nac = null, colonia = null;

            int tel=0, no_cuenta=0, puesto=0, depa=0, no_casa = 0 , cp=0;

            if (Nombre.Text == "" || CURP.Text == "" || NSS.Text == "" || RFC.Text == "" || Email.Text == ""||Contraseña.Text==""||
                Calle.Text==""||Municipio.Text==""||Estado.Text==""||Banco.Text==""||Datepuesto.Text==""||DateDpto.Text==""||
                fecHir.Text==""||fecNac.Text==""||Colonia.Text==""||Tel.Text==""||NoCuenta.Text==""||Puesto.Text==""||Dpto.Text==""||
                NoCasa.Text==""||CP.Text=="")
            {
                MessageBox.Show("Llene todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                nombre = Nombre.Text.ToString();
                curp = CURP.Text.ToString();
                nss = NSS.Text.ToString();
                rfc = RFC.Text.ToString();
                email = Email.Text.ToString();
                pass = Contraseña.Text.ToString();
                calle = Calle.Text.ToString();
                municipio = Municipio.Text.ToString();
                estado = Estado.Text.ToString();
                banco = Banco.Text.ToString();
                fec_pue = Datepuesto.Text.ToString();
                fec_dpto = DateDpto.Text.ToString();
                fec_hired = fecHir.Text.ToString();
                fec_nac = fecNac.Text.ToString();
                colonia = Colonia.Text.ToString();
                tel = int.Parse(Tel.Text.ToString());
                no_cuenta = int.Parse(NoCuenta.Text.ToString());
                puesto = int.Parse(Puesto.Text.ToString());
                depa = int.Parse(Dpto.Text.ToString());
                no_casa = int.Parse(NoCasa.Text.ToString());
                cp = int.Parse(CP.Text.ToString());

                EnlaceDB con = new EnlaceDB();
                con.Add_Dom("ID", no_casa, calle, cp, colonia, municipio, estado);

                var tabla = con.get_Dom("XD");
                dgvDom.DataSource = tabla;
                for (int i = 0; i < dgvDom.RowCount; i++)
                {
                    aux.id_dom = int.Parse(dgvDom.Rows[i].Cells[0].Value.ToString());
                }

                con.Add_Datos("I", nombre, fec_nac, curp, nss, rfc, email, tel, pass);

                var tabla2 = con.get_Datos("X");
                dgvDatos.DataSource = tabla2;
                for (int i = 0; i < dgvDatos.RowCount - 1; i++)
                {
                    aux.id_datos = int.Parse(dgvDatos.Rows[i].Cells[0].Value.ToString());
                }

                con.Add_Banco("I", banco, no_cuenta);

                var tabla3 = con.get_Banco("X");
                dgvBanco.DataSource = tabla3;
                for (int i = 0; i < dgvBanco.RowCount - 1; i++)
                {
                    aux.id_banco = int.Parse(dgvBanco.Rows[i].Cells[0].Value.ToString());
                }

                con.Add_Empleado("I", aux.id_datos, aux.id_dom, aux.id_banco, aux.id_empresa, puesto, depa, fec_dpto, fec_hired, fec_pue);

                var tabla4 = con.get_Empleado("X", aux.id_empresa);
                dgvEmpleado.DataSource = tabla4;
            }

        }//add

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            //Delete

            int id = int.Parse(dgvEmpleado.CurrentRow.Cells[0].Value.ToString());
            int datos =int.Parse(dgvEmpleado.CurrentRow.Cells[1].Value.ToString());
            int dom =int.Parse(dgvEmpleado.CurrentRow.Cells[3].Value.ToString());
            int banco =int.Parse(dgvEmpleado.CurrentRow.Cells[4].Value.ToString());

            //datos=1, dom=3, banco=4

            EnlaceDB con = new EnlaceDB();
            con.Delete_Empleado("D", id);
            con.Delete_Banco("D", banco);
            con.Delete_Dom("DD", dom);
            con.Delete_Datos("D", datos);
            

            var tabla4 = con.get_Empleado("X",aux.id_empresa);
            var tabla = con.get_Banco("X");
            var tabla2 = con.get_Datos("X");
            var tabla3 = con.get_Dom("XD");
           

            dgvBanco.DataSource = tabla;
            dgvDatos.DataSource = tabla2;
            dgvDom.DataSource = tabla3;
            dgvEmpleado.DataSource = tabla4;


            //var tabla2 = con.get_Empleado("X");
            //dgvEmpleado.DataSource = tabla2;

        }//delete

        private void button1_Click(object sender, EventArgs e)
        {
            aux.id_datos =int.Parse( dgvEmpleado.CurrentRow.Cells[1].Value.ToString());
            aux.id_dom = int.Parse(dgvEmpleado.CurrentRow.Cells[3].Value.ToString());
            aux.id_banco = int.Parse(dgvEmpleado.CurrentRow.Cells[4].Value.ToString());
            aux.id_emp = int.Parse(dgvEmpleado.CurrentRow.Cells[0].Value.ToString());

            Form8 mod = new Form8();
            mod.Show();
            this.Hide();
        }//modify

        private void button4_Click(object sender, EventArgs e)
        {
            //Form5 Incidencias= new Form5();
            //Incidencias.Show();

        }

        private void fecNac_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
