﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
	public partial class Form4 : Form
	{
		public Form4()
		{
			InitializeComponent();
		}

        private void Form4_Load(object sender, EventArgs e)
        {
            EnlaceCassandra con = new EnlaceCassandra();
            dgvDepto.DataSource= con.consultaDepto(aux.emp);

            for (int i=0;i< dgvDepto.RowCount; i++)
			{
                cbDepto.Items.Add(dgvDepto.Rows[i].Cells["nombre_dpto"].Value.ToString());
            }

        }

        private void Add_Click(object sender, EventArgs e)
        {

            string nombre = null;
            float lvl = 0;

            if (name.Text == "" ||
            Lvl.Text == ""||
            cbDepto.Text=="")
            {
                MessageBox.Show("Tiene que llenar todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                nombre = name.Text;
                lvl = float.Parse(Lvl.Text);

                EnlaceCassandra con = new EnlaceCassandra();
                con.AddPuesto(Guid.NewGuid(), nombre, lvl, cbDepto.Text);

                MessageBox.Show("Puesto agregado correctamente");

                dgvPuestos.DataSource = con.consultaPuesto(cbDepto.Text);
                dgvPuestos.Columns[3].Visible = false;

                Lvl.Text = "";
                name.Text = "";
            }
        }
    
        private void Delete_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Estas seguro de eliminar este puesto?", "Warning!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                EnlaceCassandra con = new EnlaceCassandra();
                con.DeletePuesto(Guid.Parse(dgvPuestos.CurrentRow.Cells["id_puesto"].Value.ToString()));



                Lvl.Text = "";
                name.Text = "";

                dgvPuestos.DataSource = con.consultaPuesto(cbDepto.Text);
                dgvPuestos.Columns[3].Visible = false;

            }
        }
        private void Employee_Click(object sender, EventArgs e)
        {
            Form3 Empleado = new Form3();
            Empleado.Show();
            this.Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

		private void cbDepto_SelectedIndexChanged(object sender, EventArgs e)
		{
            EnlaceCassandra con = new EnlaceCassandra();
            dgvPuestos.DataSource = con.consultaPuesto(cbDepto.Text);
            dgvPuestos.Columns[3].Visible = false;
        }

		private void Modify_Click(object sender, EventArgs e)
		{
            string nombre = null;
            float lvl = 0;

            if (name.Text == "" ||
            Lvl.Text == "" ||
            cbDepto.Text == "")
            {
                MessageBox.Show("Tiene que llenar todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                nombre = name.Text;
                lvl = float.Parse(Lvl.Text);

                EnlaceCassandra con = new EnlaceCassandra();
                con.AddPuesto(Guid.Parse(dgvPuestos.CurrentRow.Cells["id_puesto"].Value.ToString()), nombre, lvl, cbDepto.Text);

                MessageBox.Show("Puesto agregado correctamente");

                dgvPuestos.DataSource = con.consultaPuesto(cbDepto.Text);
                dgvPuestos.Columns[3].Visible = false;

                Lvl.Text = "";
                name.Text = "";
            }
        }

		private void dgvPuestos_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvPuestos.Rows[e.RowIndex];

                name.Text = dgvPuestos.CurrentRow.Cells["nombre_puesto"].Value.ToString();
                Lvl.Text = dgvPuestos.CurrentRow.Cells["lvl_salarial"].Value.ToString();

            }
        }

		private void departamentosToolStripMenuItem_Click(object sender, EventArgs e)
		{
            Form2 Dpto = new Form2();
            Dpto.Show();
            this.Hide();
		}

		private void toolStripMenuItem1_Click(object sender, EventArgs e)
		{
            Form6 Emp = new Form6();
            Emp.Show();
            this.Hide();
		}
	}
}
