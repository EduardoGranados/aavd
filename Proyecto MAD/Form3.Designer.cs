﻿namespace Proyecto_MAD
{
	partial class Form3
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.dgvEmpleado = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.TextBox();
            this.CURP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fecNac = new System.Windows.Forms.DateTimePicker();
            this.NSS = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.RFC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Tel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Contraseña = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Empresa = new System.Windows.Forms.Label();
            this.NoCuenta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Banco = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Calle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Colonia = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CP = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.NoCasa = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.Municipio = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Dpto = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Puesto = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Datepuesto = new System.Windows.Forms.DateTimePicker();
            this.label23 = new System.Windows.Forms.Label();
            this.DateDpto = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.fecHir = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.dgvDom = new System.Windows.Forms.DataGridView();
            this.dgvDatos = new System.Windows.Forms.DataGridView();
            this.dgvBanco = new System.Windows.Forms.DataGridView();
            this.dgvDpto = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.dgvPuesto = new System.Windows.Forms.DataGridView();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDpto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPuesto)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEmpleado
            // 
            this.dgvEmpleado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmpleado.Location = new System.Drawing.Point(36, 54);
            this.dgvEmpleado.Margin = new System.Windows.Forms.Padding(4);
            this.dgvEmpleado.Name = "dgvEmpleado";
            this.dgvEmpleado.ReadOnly = true;
            this.dgvEmpleado.RowHeadersWidth = 51;
            this.dgvEmpleado.Size = new System.Drawing.Size(939, 257);
            this.dgvEmpleado.TabIndex = 1;
            this.dgvEmpleado.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1191, 746);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(95, 31);
            this.button3.TabIndex = 11;
            this.button3.Text = "Borrar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(976, 746);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 31);
            this.button2.TabIndex = 10;
            this.button2.Text = "Agregar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1073, 746);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 31);
            this.button1.TabIndex = 9;
            this.button1.Text = "Modificar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(992, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Datos personales";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(992, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nombre";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(995, 82);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(218, 22);
            this.Nombre.TabIndex = 14;
            this.Nombre.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // CURP
            // 
            this.CURP.Location = new System.Drawing.Point(995, 132);
            this.CURP.Name = "CURP";
            this.CURP.Size = new System.Drawing.Size(218, 22);
            this.CURP.TabIndex = 16;
            this.CURP.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(992, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "CURP";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(992, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Fecha Nacimiento";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fecNac
            // 
            this.fecNac.Location = new System.Drawing.Point(995, 185);
            this.fecNac.Name = "fecNac";
            this.fecNac.Size = new System.Drawing.Size(265, 22);
            this.fecNac.TabIndex = 18;
            this.fecNac.ValueChanged += new System.EventHandler(this.fecNac_ValueChanged);
            // 
            // NSS
            // 
            this.NSS.Location = new System.Drawing.Point(995, 239);
            this.NSS.Name = "NSS";
            this.NSS.Size = new System.Drawing.Size(218, 22);
            this.NSS.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(992, 219);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "NSS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(995, 289);
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(218, 22);
            this.RFC.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(992, 269);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 17);
            this.label6.TabIndex = 21;
            this.label6.Text = "RFC";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(995, 341);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(218, 22);
            this.Email.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(992, 321);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Email";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Tel
            // 
            this.Tel.Location = new System.Drawing.Point(995, 394);
            this.Tel.Name = "Tel";
            this.Tel.Size = new System.Drawing.Size(218, 22);
            this.Tel.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(992, 374);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 25;
            this.label8.Text = "Telefono";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Contraseña
            // 
            this.Contraseña.Location = new System.Drawing.Point(995, 446);
            this.Contraseña.Name = "Contraseña";
            this.Contraseña.Size = new System.Drawing.Size(218, 22);
            this.Contraseña.TabIndex = 28;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(992, 426);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 17);
            this.label9.TabIndex = 27;
            this.label9.Text = "Contraseña";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1070, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 17);
            this.label10.TabIndex = 29;
            this.label10.Text = "No. Emp";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Empresa
            // 
            this.Empresa.AutoSize = true;
            this.Empresa.Location = new System.Drawing.Point(1157, 9);
            this.Empresa.Name = "Empresa";
            this.Empresa.Size = new System.Drawing.Size(56, 17);
            this.Empresa.TabIndex = 30;
            this.Empresa.Text = "123456";
            this.Empresa.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Empresa.Click += new System.EventHandler(this.label11_Click);
            // 
            // NoCuenta
            // 
            this.NoCuenta.Location = new System.Drawing.Point(995, 502);
            this.NoCuenta.Name = "NoCuenta";
            this.NoCuenta.Size = new System.Drawing.Size(218, 22);
            this.NoCuenta.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(992, 482);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 17);
            this.label12.TabIndex = 31;
            this.label12.Text = "No. cuenta";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Banco
            // 
            this.Banco.Location = new System.Drawing.Point(36, 573);
            this.Banco.Name = "Banco";
            this.Banco.Size = new System.Drawing.Size(218, 22);
            this.Banco.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(33, 553);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 17);
            this.label13.TabIndex = 33;
            this.label13.Text = "Nombre banco";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(36, 621);
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(218, 22);
            this.Calle.TabIndex = 36;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(33, 601);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 17);
            this.label15.TabIndex = 35;
            this.label15.Text = "Calle";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(281, 573);
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(218, 22);
            this.Colonia.TabIndex = 38;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(278, 553);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 17);
            this.label14.TabIndex = 37;
            this.label14.Text = "Colonia";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CP
            // 
            this.CP.Location = new System.Drawing.Point(281, 621);
            this.CP.Name = "CP";
            this.CP.Size = new System.Drawing.Size(100, 22);
            this.CP.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(278, 601);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 17);
            this.label16.TabIndex = 39;
            this.label16.Text = "CP";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // NoCasa
            // 
            this.NoCasa.Location = new System.Drawing.Point(399, 621);
            this.NoCasa.Name = "NoCasa";
            this.NoCasa.Size = new System.Drawing.Size(100, 22);
            this.NoCasa.TabIndex = 42;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(396, 601);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 17);
            this.label17.TabIndex = 41;
            this.label17.Text = "No. Casa";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(522, 573);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(218, 22);
            this.Estado.TabIndex = 44;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(519, 553);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 17);
            this.label18.TabIndex = 43;
            this.label18.Text = "Estado";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Municipio
            // 
            this.Municipio.Location = new System.Drawing.Point(522, 621);
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(218, 22);
            this.Municipio.TabIndex = 46;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(519, 601);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 17);
            this.label19.TabIndex = 45;
            this.label19.Text = "Municipio";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Dpto
            // 
            this.Dpto.Location = new System.Drawing.Point(763, 621);
            this.Dpto.Name = "Dpto";
            this.Dpto.Size = new System.Drawing.Size(218, 22);
            this.Dpto.TabIndex = 49;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(760, 601);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(98, 17);
            this.label20.TabIndex = 48;
            this.label20.Text = "Departamento";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Puesto
            // 
            this.Puesto.Location = new System.Drawing.Point(763, 573);
            this.Puesto.Name = "Puesto";
            this.Puesto.Size = new System.Drawing.Size(218, 22);
            this.Puesto.TabIndex = 47;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(760, 548);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 17);
            this.label21.TabIndex = 50;
            this.label21.Text = "Puesto ";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Datepuesto
            // 
            this.Datepuesto.Location = new System.Drawing.Point(39, 688);
            this.Datepuesto.Name = "Datepuesto";
            this.Datepuesto.Size = new System.Drawing.Size(265, 22);
            this.Datepuesto.TabIndex = 55;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(36, 668);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(131, 17);
            this.label23.TabIndex = 54;
            this.label23.Text = "Fecha Inicio Puesto";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // DateDpto
            // 
            this.DateDpto.Location = new System.Drawing.Point(321, 688);
            this.DateDpto.Name = "DateDpto";
            this.DateDpto.Size = new System.Drawing.Size(265, 22);
            this.DateDpto.TabIndex = 57;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(318, 668);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(117, 17);
            this.label24.TabIndex = 56;
            this.label24.Text = "Fecha Inicio Dpto";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fecHir
            // 
            this.fecHir.Location = new System.Drawing.Point(607, 688);
            this.fecHir.Name = "fecHir";
            this.fecHir.Size = new System.Drawing.Size(265, 22);
            this.fecHir.TabIndex = 59;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(604, 668);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(143, 17);
            this.label25.TabIndex = 58;
            this.label25.Text = "Fecha Inicio Empresa";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgvDom
            // 
            this.dgvDom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDom.Location = new System.Drawing.Point(1027, 589);
            this.dgvDom.Name = "dgvDom";
            this.dgvDom.RowHeadersWidth = 51;
            this.dgvDom.RowTemplate.Height = 24;
            this.dgvDom.Size = new System.Drawing.Size(22, 12);
            this.dgvDom.TabIndex = 60;
            this.dgvDom.Visible = false;
            // 
            // dgvDatos
            // 
            this.dgvDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos.Location = new System.Drawing.Point(1089, 589);
            this.dgvDatos.Name = "dgvDatos";
            this.dgvDatos.ReadOnly = true;
            this.dgvDatos.RowHeadersWidth = 51;
            this.dgvDatos.RowTemplate.Height = 24;
            this.dgvDatos.Size = new System.Drawing.Size(22, 12);
            this.dgvDatos.TabIndex = 61;
            this.dgvDatos.Visible = false;
            // 
            // dgvBanco
            // 
            this.dgvBanco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBanco.Location = new System.Drawing.Point(1027, 631);
            this.dgvBanco.Name = "dgvBanco";
            this.dgvBanco.ReadOnly = true;
            this.dgvBanco.RowHeadersWidth = 51;
            this.dgvBanco.RowTemplate.Height = 24;
            this.dgvBanco.Size = new System.Drawing.Size(22, 12);
            this.dgvBanco.TabIndex = 62;
            this.dgvBanco.Visible = false;
            // 
            // dgvDpto
            // 
            this.dgvDpto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDpto.Location = new System.Drawing.Point(36, 341);
            this.dgvDpto.Margin = new System.Windows.Forms.Padding(4);
            this.dgvDpto.Name = "dgvDpto";
            this.dgvDpto.ReadOnly = true;
            this.dgvDpto.RowHeadersWidth = 51;
            this.dgvDpto.Size = new System.Drawing.Size(437, 183);
            this.dgvDpto.TabIndex = 63;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(524, 320);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 17);
            this.label11.TabIndex = 65;
            this.label11.Text = "Puesto";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgvPuesto
            // 
            this.dgvPuesto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPuesto.Location = new System.Drawing.Point(527, 341);
            this.dgvPuesto.Margin = new System.Windows.Forms.Padding(4);
            this.dgvPuesto.Name = "dgvPuesto";
            this.dgvPuesto.ReadOnly = true;
            this.dgvPuesto.RowHeadersWidth = 51;
            this.dgvPuesto.Size = new System.Drawing.Size(437, 183);
            this.dgvPuesto.TabIndex = 66;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(36, 320);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(98, 17);
            this.label22.TabIndex = 67;
            this.label22.Text = "Departamento";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(36, 33);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 17);
            this.label26.TabIndex = 68;
            this.label26.Text = "Empleado";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(190, 743);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(557, 34);
            this.button4.TabIndex = 69;
            this.button4.Text = "Agregar incidencia";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1299, 790);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.dgvPuesto);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dgvDpto);
            this.Controls.Add(this.dgvBanco);
            this.Controls.Add(this.dgvDatos);
            this.Controls.Add(this.dgvDom);
            this.Controls.Add(this.fecHir);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.DateDpto);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Datepuesto);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.Dpto);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.Puesto);
            this.Controls.Add(this.Municipio);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.NoCasa);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.CP);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Colonia);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Calle);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.Banco);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.NoCuenta);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Empresa);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Contraseña);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Tel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.NSS);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.fecNac);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CURP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvEmpleado);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empleado";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDpto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPuesto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvEmpleado;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Nombre;
        private System.Windows.Forms.TextBox CURP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker fecNac;
        private System.Windows.Forms.TextBox NSS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox RFC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Email;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Tel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Contraseña;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label Empresa;
        private System.Windows.Forms.TextBox NoCuenta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Banco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Calle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Colonia;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox CP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox NoCasa;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Estado;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Municipio;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Dpto;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Puesto;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker Datepuesto;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DateTimePicker DateDpto;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DateTimePicker fecHir;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.DataGridView dgvDom;
        private System.Windows.Forms.DataGridView dgvDatos;
        private System.Windows.Forms.DataGridView dgvBanco;
        private System.Windows.Forms.DataGridView dgvDpto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dgvPuesto;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button4;
    }
}