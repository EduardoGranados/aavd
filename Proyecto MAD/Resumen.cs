﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Resumen : Form
    {
        public Resumen()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int anio = 0;

            anio = int.Parse(textBox1.Text);
        

            EnlaceDB con = new EnlaceDB();
            var tabla3 = con.get_Resumen_Pagos(anio);
            dgvRP.DataSource = tabla3;
        }
    }
}
