﻿namespace Proyecto_MAD
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.dgvEmp = new System.Windows.Forms.DataGridView();
			this.R_soc = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.R_patr = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.R_Fed = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.fecOp = new System.Windows.Forms.DateTimePicker();
			this.Calle = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.Municipio = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.Delete = new System.Windows.Forms.Button();
			this.Add = new System.Windows.Forms.Button();
			this.Modify = new System.Windows.Forms.Button();
			this.Tel = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.Nombre = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.Colonia = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.dgvEmp)).BeginInit();
			this.SuspendLayout();
			// 
			// dgvEmp
			// 
			this.dgvEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvEmp.Location = new System.Drawing.Point(20, 18);
			this.dgvEmp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.dgvEmp.Name = "dgvEmp";
			this.dgvEmp.ReadOnly = true;
			this.dgvEmp.RowHeadersWidth = 51;
			this.dgvEmp.RowTemplate.Height = 24;
			this.dgvEmp.Size = new System.Drawing.Size(472, 277);
			this.dgvEmp.TabIndex = 0;
			this.dgvEmp.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEmp_CellContentClick);
			// 
			// R_soc
			// 
			this.R_soc.Location = new System.Drawing.Point(513, 54);
			this.R_soc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.R_soc.Name = "R_soc";
			this.R_soc.Size = new System.Drawing.Size(196, 20);
			this.R_soc.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(511, 33);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Razon social";
			// 
			// R_patr
			// 
			this.R_patr.Location = new System.Drawing.Point(513, 106);
			this.R_patr.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.R_patr.Name = "R_patr";
			this.R_patr.Size = new System.Drawing.Size(196, 20);
			this.R_patr.TabIndex = 6;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(511, 85);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(87, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Registro patronal";
			// 
			// R_Fed
			// 
			this.R_Fed.Location = new System.Drawing.Point(513, 155);
			this.R_Fed.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.R_Fed.Name = "R_Fed";
			this.R_Fed.Size = new System.Drawing.Size(196, 20);
			this.R_Fed.TabIndex = 8;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(511, 134);
			this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(81, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Registro federal";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(511, 188);
			this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(125, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Fecha inicio operaciones";
			this.label5.Click += new System.EventHandler(this.label5_Click);
			// 
			// fecOp
			// 
			this.fecOp.Location = new System.Drawing.Point(513, 211);
			this.fecOp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.fecOp.Name = "fecOp";
			this.fecOp.Size = new System.Drawing.Size(203, 20);
			this.fecOp.TabIndex = 10;
			// 
			// Calle
			// 
			this.Calle.Location = new System.Drawing.Point(20, 338);
			this.Calle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Calle.Name = "Calle";
			this.Calle.Size = new System.Drawing.Size(164, 20);
			this.Calle.TabIndex = 38;
			this.Calle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Calle_KeyPress);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(17, 322);
			this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(30, 13);
			this.label15.TabIndex = 37;
			this.label15.Text = "Calle";
			this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Municipio
			// 
			this.Municipio.Location = new System.Drawing.Point(204, 384);
			this.Municipio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Municipio.Name = "Municipio";
			this.Municipio.Size = new System.Drawing.Size(164, 20);
			this.Municipio.TabIndex = 50;
			this.Municipio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Municipio_KeyPress);
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(202, 366);
			this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(52, 13);
			this.label19.TabIndex = 49;
			this.label19.Text = "Municipio";
			this.label19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Delete
			// 
			this.Delete.Location = new System.Drawing.Point(638, 429);
			this.Delete.Name = "Delete";
			this.Delete.Size = new System.Drawing.Size(71, 25);
			this.Delete.TabIndex = 53;
			this.Delete.Text = "Borrar";
			this.Delete.UseVisualStyleBackColor = true;
			this.Delete.Click += new System.EventHandler(this.Delete_Click);
			// 
			// Add
			// 
			this.Add.Location = new System.Drawing.Point(204, 429);
			this.Add.Name = "Add";
			this.Add.Size = new System.Drawing.Size(67, 25);
			this.Add.TabIndex = 52;
			this.Add.Text = "Agregar";
			this.Add.UseVisualStyleBackColor = true;
			this.Add.Click += new System.EventHandler(this.Add_Click);
			// 
			// Modify
			// 
			this.Modify.Location = new System.Drawing.Point(436, 429);
			this.Modify.Name = "Modify";
			this.Modify.Size = new System.Drawing.Size(79, 25);
			this.Modify.TabIndex = 51;
			this.Modify.Text = "Modificar";
			this.Modify.UseVisualStyleBackColor = true;
			this.Modify.Click += new System.EventHandler(this.Modify_Click);
			// 
			// Tel
			// 
			this.Tel.Location = new System.Drawing.Point(384, 384);
			this.Tel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Tel.Name = "Tel";
			this.Tel.Size = new System.Drawing.Size(131, 20);
			this.Tel.TabIndex = 57;
			this.Tel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tel_KeyPress);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(382, 363);
			this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(49, 13);
			this.label6.TabIndex = 56;
			this.label6.Text = "Telefono";
			// 
			// Nombre
			// 
			this.Nombre.Location = new System.Drawing.Point(384, 336);
			this.Nombre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Nombre.Name = "Nombre";
			this.Nombre.Size = new System.Drawing.Size(131, 20);
			this.Nombre.TabIndex = 55;
			this.Nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Nombre_KeyPress);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(382, 315);
			this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(44, 13);
			this.label7.TabIndex = 54;
			this.label7.Text = "Nombre";
			// 
			// Colonia
			// 
			this.Colonia.Location = new System.Drawing.Point(20, 384);
			this.Colonia.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Colonia.Name = "Colonia";
			this.Colonia.Size = new System.Drawing.Size(164, 20);
			this.Colonia.TabIndex = 71;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(17, 366);
			this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(42, 13);
			this.label8.TabIndex = 70;
			this.label8.Text = "Colonia";
			this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(20, 429);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(67, 25);
			this.button1.TabIndex = 73;
			this.button1.Text = "Administrar";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form6
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(730, 492);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.Colonia);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.Tel);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.Nombre);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.Delete);
			this.Controls.Add(this.Add);
			this.Controls.Add(this.Modify);
			this.Controls.Add(this.Municipio);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.Calle);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.fecOp);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.R_Fed);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.R_patr);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.R_soc);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.dgvEmp);
			this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
			this.Name = "Form6";
			this.Text = "Empresas";
			this.Load += new System.EventHandler(this.Form6_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvEmp)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEmp;
        private System.Windows.Forms.TextBox R_soc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox R_patr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox R_Fed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker fecOp;
        private System.Windows.Forms.TextBox Calle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox Municipio;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.TextBox Tel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Nombre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Colonia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
    }
}