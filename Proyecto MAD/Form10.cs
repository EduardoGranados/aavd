﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Form10 : Form
    {
        public Form10()
        {
            InitializeComponent();
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            EnlaceDB con = new EnlaceDB();
            var tabla = con.get_Dom("XD");
            dgvDomicilio.DataSource = tabla;
        }

        private void Accept_Click(object sender, EventArgs e)
        {
            int num=0, cp=0;
            string calle=null, colonia = null, municipio = null, estado = null;

            if (NoCasa.Text == "")
            {
                for (int i = 0; i < dgvDomicilio.RowCount - 1; i++)
                {
                    if (int.Parse(dgvDomicilio.Rows[i].Cells[0].Value.ToString()) == aux.id_dom)
                    {
                        num = int.Parse(dgvDomicilio.Rows[i].Cells[2].Value.ToString());
                    }
                }
            }
            else { num = int.Parse(NoCasa.Text); }
            if (CP.Text == "")
            {
                for (int i = 0; i < dgvDomicilio.RowCount - 1; i++)
                {
                    if (int.Parse(dgvDomicilio.Rows[i].Cells[0].Value.ToString()) == aux.id_dom)
                    {
                        cp = int.Parse(dgvDomicilio.Rows[i].Cells[6].Value.ToString());
                    }
                }
            }
            else { cp = int.Parse(CP.Text); }
            if (Calle.Text == "")
            {
                for (int i = 0; i < dgvDomicilio.RowCount - 1; i++)
                {
                    if (int.Parse(dgvDomicilio.Rows[i].Cells[0].Value.ToString()) == aux.id_dom)
                    {
                        calle = dgvDomicilio.Rows[i].Cells[1].Value.ToString();
                    }
                }
            }
            else { calle = Calle.Text; }
            if (Colonia.Text == "")
            {
                for (int i = 0; i < dgvDomicilio.RowCount - 1; i++)
                {
                    if (int.Parse(dgvDomicilio.Rows[i].Cells[0].Value.ToString()) == aux.id_dom)
                    {
                        colonia = dgvDomicilio.Rows[i].Cells[3].Value.ToString();
                    }
                }
            }
            else { colonia = Colonia.Text; }
            if (Municipio.Text == "")
            {
                for (int i = 0; i < dgvDomicilio.RowCount - 1; i++)
                {
                    if (int.Parse(dgvDomicilio.Rows[i].Cells[0].Value.ToString()) == aux.id_dom)
                    {
                        municipio = dgvDomicilio.Rows[i].Cells[4].Value.ToString();
                    }
                }
            }
            else { municipio = Municipio.Text; }
            if (Estado.Text == "")
            {
                for (int i = 0; i < dgvDomicilio.RowCount - 1; i++)
                {
                    if (int.Parse(dgvDomicilio.Rows[i].Cells[0].Value.ToString()) == aux.id_dom)
                    {
                        estado = dgvDomicilio.Rows[i].Cells[5].Value.ToString();
                    }
                }
            }
            else { estado = Estado.Text; }

            //validar que se llene todo

            EnlaceDB con = new EnlaceDB();
            con.update_Dom("UD",
               aux.id_dom,
               calle,
               colonia,
               num,
               municipio,
               estado,
               cp);

            var tabla = con.get_Dom("XD");
            dgvDomicilio.DataSource = tabla;

            //Desactivar boton al final 
            Accept.Enabled = false;
            Next.Enabled = true;
        }

        private void Next_Click(object sender, EventArgs e)
        {
            Form8 mod = new Form8();
            mod.Show();
            this.Hide();
        }
    }
}
