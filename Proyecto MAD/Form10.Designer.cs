﻿namespace Proyecto_MAD
{
    partial class Form10
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvDomicilio = new System.Windows.Forms.DataGridView();
            this.Municipio = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Estado = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.NoCasa = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.CP = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Colonia = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.Calle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.Next = new System.Windows.Forms.Button();
            this.Accept = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomicilio)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvDomicilio
            // 
            this.dgvDomicilio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDomicilio.Location = new System.Drawing.Point(27, 21);
            this.dgvDomicilio.Name = "dgvDomicilio";
            this.dgvDomicilio.ReadOnly = true;
            this.dgvDomicilio.RowHeadersWidth = 51;
            this.dgvDomicilio.RowTemplate.Height = 24;
            this.dgvDomicilio.Size = new System.Drawing.Size(460, 356);
            this.dgvDomicilio.TabIndex = 0;
            // 
            // Municipio
            // 
            this.Municipio.Location = new System.Drawing.Point(536, 217);
            this.Municipio.Name = "Municipio";
            this.Municipio.Size = new System.Drawing.Size(218, 22);
            this.Municipio.TabIndex = 58;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(533, 197);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 17);
            this.label19.TabIndex = 57;
            this.label19.Text = "Municipio";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Estado
            // 
            this.Estado.Location = new System.Drawing.Point(536, 279);
            this.Estado.Name = "Estado";
            this.Estado.Size = new System.Drawing.Size(218, 22);
            this.Estado.TabIndex = 56;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(533, 259);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 17);
            this.label18.TabIndex = 55;
            this.label18.Text = "Estado";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // NoCasa
            // 
            this.NoCasa.Location = new System.Drawing.Point(536, 97);
            this.NoCasa.Name = "NoCasa";
            this.NoCasa.Size = new System.Drawing.Size(100, 22);
            this.NoCasa.TabIndex = 54;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(533, 77);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 17);
            this.label17.TabIndex = 53;
            this.label17.Text = "No. Casa";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CP
            // 
            this.CP.Location = new System.Drawing.Point(654, 97);
            this.CP.Name = "CP";
            this.CP.Size = new System.Drawing.Size(100, 22);
            this.CP.TabIndex = 52;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(651, 77);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 17);
            this.label16.TabIndex = 51;
            this.label16.Text = "CP";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Colonia
            // 
            this.Colonia.Location = new System.Drawing.Point(536, 155);
            this.Colonia.Name = "Colonia";
            this.Colonia.Size = new System.Drawing.Size(218, 22);
            this.Colonia.TabIndex = 50;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(533, 135);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 17);
            this.label14.TabIndex = 49;
            this.label14.Text = "Colonia";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Calle
            // 
            this.Calle.Location = new System.Drawing.Point(536, 46);
            this.Calle.Name = "Calle";
            this.Calle.Size = new System.Drawing.Size(218, 22);
            this.Calle.TabIndex = 48;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(533, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 17);
            this.label15.TabIndex = 47;
            this.label15.Text = "Calle";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Next
            // 
            this.Next.Enabled = false;
            this.Next.Location = new System.Drawing.Point(27, 406);
            this.Next.Margin = new System.Windows.Forms.Padding(4);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(89, 31);
            this.Next.TabIndex = 61;
            this.Next.Text = "Volver";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Accept
            // 
            this.Accept.Location = new System.Drawing.Point(665, 406);
            this.Accept.Margin = new System.Windows.Forms.Padding(4);
            this.Accept.Name = "Accept";
            this.Accept.Size = new System.Drawing.Size(89, 31);
            this.Accept.TabIndex = 59;
            this.Accept.Text = "Aceptar";
            this.Accept.UseVisualStyleBackColor = true;
            this.Accept.Click += new System.EventHandler(this.Accept_Click);
            // 
            // Form10
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Next);
            this.Controls.Add(this.Accept);
            this.Controls.Add(this.Municipio);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.Estado);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.NoCasa);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.CP);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.Colonia);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Calle);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.dgvDomicilio);
            this.Name = "Form10";
            this.Text = "Direccion";
            this.Load += new System.EventHandler(this.Form10_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDomicilio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvDomicilio;
        private System.Windows.Forms.TextBox Municipio;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Estado;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox NoCasa;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox CP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Colonia;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Calle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Accept;
    }
}