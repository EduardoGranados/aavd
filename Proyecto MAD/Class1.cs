﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_MAD
{
    public class Empresa
    {
        public Guid id_emp { get; set; }
        public string razon_social { get; set; }
        public string calle { get; set; }
        public string municipio { get; set; }
        public string colonia { get; set; }
        public string fec_ini_op { get; set; }
        public string reg_patronal { get; set; }
        public string reg_federal { get; set; }
        public long tel { get; set; }
        public string nombre_empresa { get; set; }
    }

    public class Frecuencia
    {
        public Guid id_frecuencia { get; set; }
        public bool semanal { get; set; }
        public bool catorcenal { get; set; }
        public bool quincenal { get; set; }
        public bool mensual { get; set; }

    }

    public class Deducciones
    {
        public Guid id_deducciones { get; set; }
        public string concepto { get; set; }
        public bool porcentaje { get; set; }
        public bool cant { get; set; }
        public float cuota { get; set; }

    }
    public class Puesto
    {
        public Guid id_puesto { get; set; }
        public string nombre_puesto { get; set; }
        public float lvl_salarial { get; set; }
        public string nombre_dpto { get; set; }

    }

    public class Incidencias
    {
        public Guid id_incidencias { get; set; }
        public string concepto { get; set; }
        public bool cantidad { get; set; }
        public bool porcentaje { get; set; }
        public float cuota { get; set; }
    }

    public class Nomina
    {
        public Guid id_nomina { get; set; }
        public string fecha { get; set; }
        public float deducciones { get; set; }
        public float percepciones { get; set; }
        public float s_bruto { get; set; }
        public float s_neto { get; set; }

    }
    public class Percepciones
    {
        public Guid id_percepciones { get; set; }
        public string concepto { get; set; }
        public bool cant { get; set; }
        public bool porcentaje { get; set; }
        public float cuota { get; set; }
        public float ISR { get; set; }
        public float p_IMSS { get; set; }
    }

    public class Departamento
    {
        public Guid id_dpto { get; set; }
        public string nombre_dpto { get; set; }
        public float s_base { get; set; }
        public Guid id_emp { get; set; }
    }

    public class Empleado
    {
        public Guid id_empleado { get; set; }
        public float sueldo_neto { get; set; }
        public string calle { get; set; }
        public string municipio { get; set; }
        public string colonia { get; set; }
        public int no_cuenta { get; set; }
        public string nombre_banco { get; set; }
        public string departamento { get; set; }
        public string puesto { get; set; }
        public string fecha_hired { get; set; }
        public string fecha_nacimiento { get; set; }
        public float s_diario { get; set; }
        public string fecha_puesto { get; set; }
        public string fecha_dpto { get; set; }
        public string empresa { get; set; }

    }

    public class Resumen_pagos_by_Empleados
    {
        public Guid ReciboEmp { get; set; }
        public int id_empleado { get; set; }
        public int anio { get; set; }
        public string RFC { get; set; }
        public int NSS { get; set; }
        public int mes { get; set; }
        public float p_IMSS { get; set; }
        public float ISR { get; set; }
        public float sueldo_bruto { get; set; }
        public float sueldo_neto { get; set; }

    }
    public class Recibo_nomina_by_Empleados
    {
        public Guid ReciboEmp { get; set; }
        public int id_empleado { get; set; }
        public int id_empresa { get; set; }
        public string nombre_empresa { get; set; }
        public int anio { get; set; }
        public string RFC { get; set; }
        public string CURP { get; set; }
        public int NSS { get; set; }
        public int mes { get; set; }
        public float p_IMSS { get; set; }
        public string puesto { get; set; }
        public string departamento { get; set; }
        public float salario_diario { get; set; }
        public string fecha_inicio_operaciones { get; set; }
        public string registro_patronal { get; set; }
        public string fecpago { get; set; }
        public string frecuencia { get; set; }
        public int id_percepciones { get; set; }
        public int id_deducciones { get; set; }
        public string concepto_deduccion { get; set; }
        public string concepto_percepciones { get; set; }
        public float cuota_percepciones { get; set; }
        public float cuota_deducciones { get; set; }

    }

    public class Reporte_gral_by_gerente
    {
        public int id_empresa { get; set; }
        public int id_empleado { get; set; }
        public int anio { get; set; }
        public int mes { get; set; }
        public string dpto { get; set; }
        public string puesto { get; set; }
        public string nombre_empleado { get; set; }
        public string fec_ini_empleado { get; set; }
        public string fecnac { get; set; }
        public float s_diario { get; set; }

    }

    public class Reporte_headcounter1_by_gerente
    {
        public int id_empresa { get; set; }
        public string dpto { get; set; }
        public string anio_mes { get; set; }
        public string gerente { get; set; }
        public string puesto { get; set; }
        public int cant_emp_puesto { get; set; }
    }
    public class Reporte_headcounter2_by_gerente
    {
        public int id_empresa { get; set; }
        public string dpto { get; set; }
        public int anio { get; set; }
        public int mes { get; set; }
        public string gerente { get; set; }
        public string fecnomina { get; set; }
        public int cant_emp_dpto { get; set; }
    }
    public class Reporte_nomina_by_gerente
    {
        public int id_empresa { get; set; }
        public string dpto { get; set; }
        public int anio_mes { get; set; }
        public string gerente { get; set; }
        public float p_IMSS { get; set; }
        public float ISR { get; set; }
        public float s_bruto { get; set; }
        public float s_neto { get; set; }
        public float deducciones { get; set; }
        public float percepciones { get; set; }
        public float total { get; set; }
    }
}