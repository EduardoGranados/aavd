﻿namespace Proyecto_MAD
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.Contraseña = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Tel = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.RFC = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.NSS = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.fechNac = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.CURP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Accept = new System.Windows.Forms.Button();
            this.Volver = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvData
            // 
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(30, 23);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersWidth = 51;
            this.dgvData.RowTemplate.Height = 24;
            this.dgvData.Size = new System.Drawing.Size(480, 388);
            this.dgvData.TabIndex = 0;
            // 
            // Contraseña
            // 
            this.Contraseña.Location = new System.Drawing.Point(531, 407);
            this.Contraseña.Name = "Contraseña";
            this.Contraseña.Size = new System.Drawing.Size(218, 22);
            this.Contraseña.TabIndex = 44;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(528, 387);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 17);
            this.label9.TabIndex = 43;
            this.label9.Text = "Contraseña";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Tel
            // 
            this.Tel.Location = new System.Drawing.Point(531, 355);
            this.Tel.Name = "Tel";
            this.Tel.Size = new System.Drawing.Size(218, 22);
            this.Tel.TabIndex = 42;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(528, 335);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 41;
            this.label8.Text = "Telefono";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Email
            // 
            this.Email.Location = new System.Drawing.Point(531, 302);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(218, 22);
            this.Email.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(528, 282);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 17);
            this.label7.TabIndex = 39;
            this.label7.Text = "Email";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // RFC
            // 
            this.RFC.Location = new System.Drawing.Point(531, 250);
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(218, 22);
            this.RFC.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(528, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 17);
            this.label6.TabIndex = 37;
            this.label6.Text = "RFC";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // NSS
            // 
            this.NSS.Location = new System.Drawing.Point(531, 200);
            this.NSS.Name = "NSS";
            this.NSS.Size = new System.Drawing.Size(218, 22);
            this.NSS.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(528, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 35;
            this.label5.Text = "NSS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fechNac
            // 
            this.fechNac.Location = new System.Drawing.Point(531, 146);
            this.fechNac.Name = "fechNac";
            this.fechNac.Size = new System.Drawing.Size(218, 22);
            this.fechNac.TabIndex = 34;
            this.fechNac.ValueChanged += new System.EventHandler(this.fechNac_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(528, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 33;
            this.label4.Text = "Fecha Nacimiento";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CURP
            // 
            this.CURP.Location = new System.Drawing.Point(531, 93);
            this.CURP.Name = "CURP";
            this.CURP.Size = new System.Drawing.Size(218, 22);
            this.CURP.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(528, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "CURP";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Nombre
            // 
            this.Nombre.Location = new System.Drawing.Point(531, 43);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(218, 22);
            this.Nombre.TabIndex = 30;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(528, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 29;
            this.label2.Text = "Nombre";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Accept
            // 
            this.Accept.Location = new System.Drawing.Point(601, 450);
            this.Accept.Margin = new System.Windows.Forms.Padding(4);
            this.Accept.Name = "Accept";
            this.Accept.Size = new System.Drawing.Size(89, 31);
            this.Accept.TabIndex = 45;
            this.Accept.Text = "Aceptar";
            this.Accept.UseVisualStyleBackColor = true;
            this.Accept.Click += new System.EventHandler(this.Accept_Click);
            // 
            // Volver
            // 
            this.Volver.Enabled = false;
            this.Volver.Location = new System.Drawing.Point(56, 450);
            this.Volver.Margin = new System.Windows.Forms.Padding(4);
            this.Volver.Name = "Volver";
            this.Volver.Size = new System.Drawing.Size(89, 31);
            this.Volver.TabIndex = 46;
            this.Volver.Text = "Volver";
            this.Volver.UseVisualStyleBackColor = true;
            this.Volver.Click += new System.EventHandler(this.Volver_Click);
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 494);
            this.Controls.Add(this.Volver);
            this.Controls.Add(this.Accept);
            this.Controls.Add(this.Contraseña);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Tel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.RFC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.NSS);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.fechNac);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CURP);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvData);
            this.Name = "Form9";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Datos";
            this.Load += new System.EventHandler(this.Form9_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.TextBox Contraseña;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox Tel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Email;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox RFC;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox NSS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker fechNac;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CURP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Nombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Accept;
        private System.Windows.Forms.Button Volver;
    }
}