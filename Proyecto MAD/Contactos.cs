﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Contactos : Form
    {
        public Contactos()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Delete_Click(object sender, EventArgs e)
        {
            int id = int.Parse(dgvCts.CurrentRow.Cells[0].Value.ToString());

            EnlaceDB con = new EnlaceDB();
            var tabla2 = con.Delete_Cts("DC",
                id);
            dgvCts.DataSource = tabla2;

            var tabla1 = con.get_Cts("XC");
            dgvCts.DataSource = tabla1;
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void Contactos_Load(object sender, EventArgs e)
        {
            EnlaceDB con = new EnlaceDB();
            var tabla2 = con.get_Cts("XC");
            dgvCts.DataSource = tabla2;
        }

        private void dgvCts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            //int id = int.Parse(dgvCts.CurrentRow.Cells[0].Value.ToString());
            string nombre = null;
            int tel = 0;

            nombre = textBox1.Text;
            tel = int.Parse(textBox2.Text);

            EnlaceDB con = new EnlaceDB();
            var tabla = con.add_Cts("IC",
                nombre,
                tel);


            var tabla2 = con.get_Cts("XC");
            dgvCts.DataSource = tabla2;

            textBox1.Text = "";
            textBox2.Text = "";

        }

        private void Modify_Click(object sender, EventArgs e)
        {
            int id = int.Parse(dgvCts.CurrentRow.Cells[0].Value.ToString());
            string nombre = null;
            int tel = 0;


            if(textBox1.Text == "")
            {
                nombre = dgvCts.CurrentRow.Cells[1].Value.ToString();
            }
            else
                nombre = textBox1.Text;

            if (textBox2.Text == "" )
            {
                tel = int.Parse(dgvCts.CurrentRow.Cells[2].Value.ToString());
            }
            else
                tel = int.Parse(textBox2.Text);


            EnlaceDB con = new EnlaceDB();
            var tabla = con.update_cts("UC",
                id,
                nombre,
                tel);


            var tabla2 = con.get_Cts("XC");
            dgvCts.DataSource = tabla2;

            textBox1.Text = "";
            textBox2.Text = "";
        }
    }
}
