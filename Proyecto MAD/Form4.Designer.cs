﻿namespace Proyecto_MAD
{
	partial class Form4
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgvPuestos = new System.Windows.Forms.DataGridView();
			this.Delete = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.name = new System.Windows.Forms.TextBox();
			this.Lvl = new System.Windows.Forms.TextBox();
			this.Add = new System.Windows.Forms.Button();
			this.Modify = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.cbDepto = new System.Windows.Forms.ComboBox();
			this.dgvDepto = new System.Windows.Forms.DataGridView();
			this.menuStrip2 = new System.Windows.Forms.MenuStrip();
			this.departamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.puestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.calcularNominaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.dgvPuestos)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvDepto)).BeginInit();
			this.menuStrip2.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgvPuestos
			// 
			this.dgvPuestos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvPuestos.Location = new System.Drawing.Point(42, 55);
			this.dgvPuestos.Name = "dgvPuestos";
			this.dgvPuestos.ReadOnly = true;
			this.dgvPuestos.RowHeadersWidth = 51;
			this.dgvPuestos.Size = new System.Drawing.Size(716, 386);
			this.dgvPuestos.TabIndex = 2;
			this.dgvPuestos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPuestos_CellContentClick);
			// 
			// Delete
			// 
			this.Delete.Location = new System.Drawing.Point(794, 402);
			this.Delete.Name = "Delete";
			this.Delete.Size = new System.Drawing.Size(130, 25);
			this.Delete.TabIndex = 19;
			this.Delete.Text = "Borrar";
			this.Delete.UseVisualStyleBackColor = true;
			this.Delete.Click += new System.EventHandler(this.Delete_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(790, 132);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(95, 20);
			this.label2.TabIndex = 26;
			this.label2.Text = "Nivel salarial";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(790, 67);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 20);
			this.label1.TabIndex = 25;
			this.label1.Text = "Nombre";
			// 
			// name
			// 
			this.name.Location = new System.Drawing.Point(794, 90);
			this.name.Name = "name";
			this.name.Size = new System.Drawing.Size(130, 20);
			this.name.TabIndex = 24;
			// 
			// Lvl
			// 
			this.Lvl.Location = new System.Drawing.Point(794, 156);
			this.Lvl.Name = "Lvl";
			this.Lvl.Size = new System.Drawing.Size(130, 20);
			this.Lvl.TabIndex = 23;
			// 
			// Add
			// 
			this.Add.Location = new System.Drawing.Point(794, 283);
			this.Add.Name = "Add";
			this.Add.Size = new System.Drawing.Size(130, 25);
			this.Add.TabIndex = 22;
			this.Add.Text = "Agregar";
			this.Add.UseVisualStyleBackColor = true;
			this.Add.Click += new System.EventHandler(this.Add_Click);
			// 
			// Modify
			// 
			this.Modify.Location = new System.Drawing.Point(794, 340);
			this.Modify.Name = "Modify";
			this.Modify.Size = new System.Drawing.Size(130, 25);
			this.Modify.TabIndex = 21;
			this.Modify.Text = "Modificar";
			this.Modify.UseVisualStyleBackColor = true;
			this.Modify.Click += new System.EventHandler(this.Modify_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(790, 193);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(112, 20);
			this.label3.TabIndex = 28;
			this.label3.Text = "Departamento";
			this.label3.Click += new System.EventHandler(this.label3_Click);
			// 
			// cbDepto
			// 
			this.cbDepto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbDepto.FormattingEnabled = true;
			this.cbDepto.Location = new System.Drawing.Point(794, 224);
			this.cbDepto.Name = "cbDepto";
			this.cbDepto.Size = new System.Drawing.Size(129, 21);
			this.cbDepto.TabIndex = 29;
			this.cbDepto.SelectedIndexChanged += new System.EventHandler(this.cbDepto_SelectedIndexChanged);
			// 
			// dgvDepto
			// 
			this.dgvDepto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDepto.Location = new System.Drawing.Point(429, 12);
			this.dgvDepto.Name = "dgvDepto";
			this.dgvDepto.ReadOnly = true;
			this.dgvDepto.RowHeadersWidth = 51;
			this.dgvDepto.Size = new System.Drawing.Size(22, 21);
			this.dgvDepto.TabIndex = 30;
			this.dgvDepto.Visible = false;
			// 
			// menuStrip2
			// 
			this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.departamentosToolStripMenuItem,
            this.puestosToolStripMenuItem,
            this.empleadosToolStripMenuItem,
            this.calcularNominaToolStripMenuItem,
            this.toolStripMenuItem1});
			this.menuStrip2.Location = new System.Drawing.Point(0, 0);
			this.menuStrip2.Name = "menuStrip2";
			this.menuStrip2.Size = new System.Drawing.Size(985, 24);
			this.menuStrip2.TabIndex = 31;
			this.menuStrip2.Text = "menuStrip2";
			// 
			// departamentosToolStripMenuItem
			// 
			this.departamentosToolStripMenuItem.Name = "departamentosToolStripMenuItem";
			this.departamentosToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
			this.departamentosToolStripMenuItem.Text = "Departamentos";
			this.departamentosToolStripMenuItem.Click += new System.EventHandler(this.departamentosToolStripMenuItem_Click);
			// 
			// puestosToolStripMenuItem
			// 
			this.puestosToolStripMenuItem.Enabled = false;
			this.puestosToolStripMenuItem.Name = "puestosToolStripMenuItem";
			this.puestosToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
			this.puestosToolStripMenuItem.Text = "Puestos";
			// 
			// empleadosToolStripMenuItem
			// 
			this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
			this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
			this.empleadosToolStripMenuItem.Text = "Empleados";
			// 
			// calcularNominaToolStripMenuItem
			// 
			this.calcularNominaToolStripMenuItem.Name = "calcularNominaToolStripMenuItem";
			this.calcularNominaToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
			this.calcularNominaToolStripMenuItem.Text = "Calcular Nomina";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(99, 20);
			this.toolStripMenuItem1.Text = "Volver Empresa";
			this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
			// 
			// Form4
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(985, 482);
			this.Controls.Add(this.menuStrip2);
			this.Controls.Add(this.dgvDepto);
			this.Controls.Add(this.cbDepto);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.name);
			this.Controls.Add(this.Lvl);
			this.Controls.Add(this.Add);
			this.Controls.Add(this.Modify);
			this.Controls.Add(this.Delete);
			this.Controls.Add(this.dgvPuestos);
			this.Name = "Form4";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Puestos";
			this.Load += new System.EventHandler(this.Form4_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvPuestos)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dgvDepto)).EndInit();
			this.menuStrip2.ResumeLayout(false);
			this.menuStrip2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvPuestos;
		private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox Lvl;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Modify;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox cbDepto;
		private System.Windows.Forms.DataGridView dgvDepto;
		private System.Windows.Forms.MenuStrip menuStrip2;
		private System.Windows.Forms.ToolStripMenuItem departamentosToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem puestosToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem calcularNominaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
	}
}