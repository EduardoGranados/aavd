﻿namespace Proyecto_MAD
{
    partial class Form12
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DateDpto = new System.Windows.Forms.DateTimePicker();
            this.label24 = new System.Windows.Forms.Label();
            this.Datepuesto = new System.Windows.Forms.DateTimePicker();
            this.label23 = new System.Windows.Forms.Label();
            this.dgvPuestos = new System.Windows.Forms.DataGridView();
            this.dgvDepto = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.FecHired = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.Next = new System.Windows.Forms.Button();
            this.Accept = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Puesto = new System.Windows.Forms.TextBox();
            this.Departamento = new System.Windows.Forms.TextBox();
            this.dgvEmp = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPuestos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmp)).BeginInit();
            this.SuspendLayout();
            // 
            // DateDpto
            // 
            this.DateDpto.Location = new System.Drawing.Point(919, 270);
            this.DateDpto.Name = "DateDpto";
            this.DateDpto.Size = new System.Drawing.Size(265, 22);
            this.DateDpto.TabIndex = 65;
            this.DateDpto.ValueChanged += new System.EventHandler(this.DateDpto_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(916, 235);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(117, 17);
            this.label24.TabIndex = 64;
            this.label24.Text = "Fecha Inicio Dpto";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Datepuesto
            // 
            this.Datepuesto.Location = new System.Drawing.Point(172, 266);
            this.Datepuesto.Name = "Datepuesto";
            this.Datepuesto.Size = new System.Drawing.Size(272, 22);
            this.Datepuesto.TabIndex = 63;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(169, 235);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(131, 17);
            this.label23.TabIndex = 62;
            this.label23.Text = "Fecha Inicio Puesto";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgvPuestos
            // 
            this.dgvPuestos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPuestos.Location = new System.Drawing.Point(16, 73);
            this.dgvPuestos.Name = "dgvPuestos";
            this.dgvPuestos.ReadOnly = true;
            this.dgvPuestos.RowHeadersWidth = 51;
            this.dgvPuestos.RowTemplate.Height = 24;
            this.dgvPuestos.Size = new System.Drawing.Size(644, 155);
            this.dgvPuestos.TabIndex = 66;
            this.dgvPuestos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dgvDepto
            // 
            this.dgvDepto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDepto.Location = new System.Drawing.Point(693, 73);
            this.dgvDepto.Name = "dgvDepto";
            this.dgvDepto.ReadOnly = true;
            this.dgvDepto.RowHeadersWidth = 51;
            this.dgvDepto.RowTemplate.Height = 24;
            this.dgvDepto.Size = new System.Drawing.Size(644, 155);
            this.dgvDepto.TabIndex = 67;
            this.dgvDepto.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDepto_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 69;
            this.label1.Text = "Puesto";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(690, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 17);
            this.label3.TabIndex = 71;
            this.label3.Text = "Departamento";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FecHired
            // 
            this.FecHired.Location = new System.Drawing.Point(573, 268);
            this.FecHired.Name = "FecHired";
            this.FecHired.Size = new System.Drawing.Size(265, 22);
            this.FecHired.TabIndex = 75;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(585, 240);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 17);
            this.label6.TabIndex = 74;
            this.label6.Text = "Fecha contratacion";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Next
            // 
            this.Next.Enabled = false;
            this.Next.Location = new System.Drawing.Point(317, 422);
            this.Next.Margin = new System.Windows.Forms.Padding(4);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(89, 31);
            this.Next.TabIndex = 77;
            this.Next.Text = "Volver";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Accept
            // 
            this.Accept.Location = new System.Drawing.Point(955, 422);
            this.Accept.Margin = new System.Windows.Forms.Padding(4);
            this.Accept.Name = "Accept";
            this.Accept.Size = new System.Drawing.Size(89, 31);
            this.Accept.TabIndex = 76;
            this.Accept.Text = "Aceptar";
            this.Accept.UseVisualStyleBackColor = true;
            this.Accept.Click += new System.EventHandler(this.Accept_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(420, 308);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 78;
            this.label2.Text = "Puesto";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(764, 308);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 79;
            this.label4.Text = "Departamento";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Puesto
            // 
            this.Puesto.Location = new System.Drawing.Point(423, 328);
            this.Puesto.Name = "Puesto";
            this.Puesto.Size = new System.Drawing.Size(218, 22);
            this.Puesto.TabIndex = 80;
            // 
            // Departamento
            // 
            this.Departamento.Location = new System.Drawing.Point(767, 328);
            this.Departamento.Name = "Departamento";
            this.Departamento.Size = new System.Drawing.Size(218, 22);
            this.Departamento.TabIndex = 81;
            // 
            // dgvEmp
            // 
            this.dgvEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmp.Location = new System.Drawing.Point(342, 28);
            this.dgvEmp.Name = "dgvEmp";
            this.dgvEmp.RowHeadersWidth = 51;
            this.dgvEmp.RowTemplate.Height = 24;
            this.dgvEmp.Size = new System.Drawing.Size(389, 104);
            this.dgvEmp.TabIndex = 82;
            this.dgvEmp.Visible = false;
            // 
            // Form12
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1360, 495);
            this.Controls.Add(this.dgvEmp);
            this.Controls.Add(this.Departamento);
            this.Controls.Add(this.Puesto);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Next);
            this.Controls.Add(this.Accept);
            this.Controls.Add(this.FecHired);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvDepto);
            this.Controls.Add(this.dgvPuestos);
            this.Controls.Add(this.DateDpto);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.Datepuesto);
            this.Controls.Add(this.label23);
            this.Name = "Form12";
            this.Text = "Agregar";
            this.Load += new System.EventHandler(this.Form12_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPuestos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker DateDpto;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DateTimePicker Datepuesto;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView dgvPuestos;
        private System.Windows.Forms.DataGridView dgvDepto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker FecHired;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Accept;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Puesto;
        private System.Windows.Forms.TextBox Departamento;
        private System.Windows.Forms.DataGridView dgvEmp;
    }
}