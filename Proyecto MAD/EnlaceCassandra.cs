﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassandra;
using Cassandra.Mapping;
using System.Configuration;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    class EnlaceCassandra
    {
        static private string _dbServer { set; get; }
        static private string _dbKeySpace { set; get; }
        static private Cluster _cluster;
        static private ISession _session;

        private static void conectar()
        {
            _dbServer = ConfigurationManager.AppSettings["Cluster"].ToString();
            _dbKeySpace = ConfigurationManager.AppSettings["KeySpace"].ToString();

            _cluster = Cluster.Builder()
                .AddContactPoint(_dbServer)
                .Build();

            _session = _cluster.Connect(_dbKeySpace);
        }


        private static void conectar2()
        {
            _cluster = Cluster.Builder()
                .AddContactPoint("127.0.0.1")
                .Build();

            _session = _cluster.Connect("keyspace3");
        }

        private static void desconectar()
        {
            _cluster.Dispose();
        }

        // Ejemplo de leer row x row
        public string ValidaUsuario(string us, string ps)
        {
            string nombre = "";
            var Pass = "";
            var Nombre = "";
            conectar();

            string query = "SELECT Pass, Nombre FROM Usuarios WHERE Email = '" + us + "';";

            // Execute a query on a connection synchronously 
            var rs = _session.Execute(query);

            // Iterate through the RowSet 
            foreach (var row in rs)
            {
                Pass = row.GetValue<string>("pass");
                Nombre = row.GetValue<string>("nombre");
                if (Pass == ps)
                    nombre = Nombre;
            }

            return nombre;
        }


        public void InsertaDatos(int id, string dato)
        {
            try
            {
                conectar();
                
                string qry = "insert into ejemplo(campo1, campo2) values(";
                qry = qry + id.ToString();
                qry = qry + ",'";
                qry = qry + dato;
                qry = qry + "');";


                string query = "insert into ejemplo(campo1, campo2) values({0}, '{1}');";
                qry = string.Format(query, id, dato);

                _session.Execute(qry);
            }
            catch(Exception e)
            {
                throw e;   
            }
            finally
            {
                // desconectar o cerrar la conexión
                desconectar();
            }
        }

        //Empresa
        public void AddEmpresa(Guid id_emp, string rsoc, string calle, string muni, string col,string fecOp, string regP, string regF,long tel,string nombre)
		{
            try
            {
                conectar();
                string qry = "INSERT INTO Empresa(id_emp, nombre_empresa , calle, municipio, colonia, fec_ini_op, reg_patronal, reg_federal, tel, razon_social) VALUES(" + id_emp + ", '" + nombre + "', '" + calle + "', '" + muni + "', '" + col + "', '" + fecOp + "', '" + regP + "', '" + regF + "',"+tel+",'"+rsoc+"');";
                _session.Execute(qry);
            }
			catch (Exception e)
			{
                throw e;
			}
			finally
			{
                desconectar();
			}
        }

        public void DeleteEmpresa(Guid id_emp)
        {
            try
            {
                conectar();
                string qry = "DELETE FROM Empresa where id_emp = " + id_emp + ";";
                _session.Execute(qry);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                desconectar();
            }
        }
        public List<Empresa> consultaEmpresa()
        {
            try
            {
                conectar();
                string query = "SELECT *FROM Empresa;";
                string def = string.Format(query);
               // ISession session = _cluster.Connect(_dbKeySpace);
                IMapper mapper = new Mapper(_session);
                IEnumerable<Empresa> sem = mapper.Fetch<Empresa>(def);
                return sem.ToList();
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
            }
        }

        //depto
        public void AddDepto(Guid id_dpto, string nombre, float sueldo, Guid emp)
        {
            try
            {
                conectar();
                string qry = "INSERT INTO Departamento(id_dpto, nombre_dpto, s_base, id_emp) VALUES(" + id_dpto + ", '" + nombre + "', " + sueldo + ", " + emp + ");";
                _session.Execute(qry);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                desconectar();
            }
        }
        public void DeleteDpto(Guid id_dpto)
        {
            try
            {
                conectar();
                string qry = "DELETE FROM Departamento where id_dpto = " + id_dpto + ";";
                _session.Execute(qry);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                desconectar();
            }
        }

        public List<Departamento> consultaDepto(Guid emp)
        {
            try
            {
                conectar();
                string query = "SELECT *FROM Departamento where id_emp="+emp+ " allow filtering;";
                string def = string.Format(query);
                // ISession session = _cluster.Connect(_dbKeySpace);
                IMapper mapper = new Mapper(_session);
                IEnumerable<Departamento> sem = mapper.Fetch<Departamento>(def);
                return sem.ToList();
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
            }
        }

        //puesto
        public void AddPuesto(Guid id_puesto, string nombre, float lvl, string dpto)
        {
            try
            {
                conectar();
                string qry = "INSERT INTO Puesto(id_puesto, nombre_puesto, lvl_salarial, nombre_dpto) VALUES(" + id_puesto + ", '" + nombre + "', " + lvl + ", '" + dpto + "');";
                _session.Execute(qry);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                desconectar();
            }
        }
        public void DeletePuesto(Guid id_puesto)
        {
            try
            {
                conectar();
                string qry = "DELETE FROM Puesto where id_puesto = " + id_puesto + ";";
                _session.Execute(qry);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                desconectar();
            }
        }

        public List<Puesto> consultaPuesto(string dpto)
        {
            try
            {
                conectar();
                string query = "SELECT *FROM Puesto where nombre_dpto='" + dpto + "' allow filtering;";
                string def = string.Format(query);
                // ISession session = _cluster.Connect(_dbKeySpace);
                IMapper mapper = new Mapper(_session);
                IEnumerable<Puesto> sem = mapper.Fetch<Puesto>(def);
                return sem.ToList();
            }
            catch (Exception e)
            {
                throw (e);
            }
            finally
            {
            }
        }




        //      public void AddEmpleados(Guid id,float sueldo,string calle,string municipio,string colonia, int nocuenta,
        //          string banco, string dpto, string puesto, string fech, string fecn, float s_diario, string fecp, string fecd,
        //          string empresa)
        //{
        //	try
        //	{
        //              conectar();
        //              string qry = "INSERT INTO EMPLEADO (id_empleado,no_empleado, password, nombrecom,empresa, departamento, puesto, nsalarial, fnacimiento, nss,curp, rfc, banco,email,tel,direccion,ciudad) VALUES (" + id_empleado + ", '" + no + "', '" + p + "', '" + nom + "', '" + empre + "', '" + depa + "', '" + puesto + "', '" + ns + "', '" + fn + "', '" + nss + "', '" + curp + "', '" + rfc + "','" + bn + "', '" + ema + "','" + tel + "',' " + dire + "',' " + cd + "');";
        //              //string f = string.Format(query, uuid, n, ap, am, c, nn, cd, fn, d, t1, t2, t3, tu)
        //              _session.Execute(qry);
        //	}
        //	catch (Exception e)
        //	{
        //              throw e;
        //	}
        //	finally
        //	{
        //              desconectar();
        //	}
        //}

        //public IEnumerable<Usuarios> Get_One(int dato)
        //{
        //    string query = "SELECT campo1, campo2 FROM ejemplo WHERE campo1 = ?;";
        //    conectar();
        //    IMapper mapper = new Mapper(_session);
        //    IEnumerable<Usuarios> users = mapper.Fetch<Usuarios>(query, dato);

        //    desconectar();
        //    return users.ToList();
        //}

        //public List<Usuarios> Get_All()
        //{
        //    string query = "SELECT campo1, campo2 FROM ejemplo;";
        //    conectar();

        //    IMapper mapper = new Mapper(_session);
        //    IEnumerable<Usuarios> users = mapper.Fetch<Usuarios>(query);

        //    desconectar();
        //    return users.ToList();

        //}

        // Ejemplo de leer row x row
        public void GetOne()
        {
            conectar();

            string query ="SELECT campo1, campo2 FROM ejemplo;";

            // Execute a query on a connection synchronously 
            var rs = _session.Execute(query);
            
            // Iterate through the RowSet 
            foreach (var row in rs)
            {
                var value = row.GetValue<int>("campo1");
                // Do something with the value 
                var texto = row.GetValue<string>("campo2");
                // Do something with the value 

                MessageBox.Show(texto, value.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                /*
                RowSet rsUsers = session.Execute(qry);

                ////////////////////////////////////////////////
                var users = new List<UserModel>();
                foreach (var userRow in rsUsers)
                {
                    //users.Add(ReflectionTools.GetSingleEntryDynamicFromReader<UserModel>(userRow));
                }

                foreach (UserModel user in users)
                {
                    Console.WriteLine("{0} {1} {2} {3} {4}", user.Id, user.FirstName, user.LastName, user.Country, user.IsActive);
                }
                */

            }
        }

    }
}
