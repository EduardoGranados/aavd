﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void Banco_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form11_Load(object sender, EventArgs e)
        {
            EnlaceDB con = new EnlaceDB();
            var tabla2 = con.get_Banco("X");
            dgvBanco.DataSource = tabla2;
        }

        private void Accept_Click(object sender, EventArgs e)
        {

            string banco = null; int cuenta = 0;

            if (Banco.Text == "")
            {
                for (int i = 0; i < dgvBanco.RowCount - 1; i++)
                {
                    if (int.Parse(dgvBanco.Rows[i].Cells[0].Value.ToString()) == aux.id_banco)
                    {
                        banco = dgvBanco.Rows[i].Cells[1].Value.ToString();
                    }
                }
            }
            else { banco = Banco.Text; }
            if (NoCuenta.Text == "")
            {
                for (int i = 0; i < dgvBanco.RowCount - 1; i++)
                {
                    if (int.Parse(dgvBanco.Rows[i].Cells[0].Value.ToString()) == aux.id_banco)
                    {
                        cuenta = int.Parse(dgvBanco.Rows[i].Cells[2].Value.ToString());
                    }
                }
            }
            else { cuenta = int.Parse(NoCuenta.Text); }

            EnlaceDB con = new EnlaceDB();
            con.update_Banco("U", aux.id_banco, cuenta, banco);

            var tabla = con.get_Banco("X");
            dgvBanco.DataSource = tabla;

            Accept.Enabled = false;
            Next.Enabled = true;

            //banco = Banco.Text;
            //cuenta = int.Parse(NoCuenta.Text);

            //EnlaceDB con = new EnlaceDB();
            //var tabla2 = con.Add_Banco("I",
            //    banco,
            //    cuenta);
            //dgvBanco.DataSource = tabla2;

            //Banco.Text = "";
            //NoCuenta.Text = "";
        }

        private void Next_Click(object sender, EventArgs e)
        {
            Form8 mod = new Form8();
            mod.Show();
            this.Hide();
        }
    }
}
