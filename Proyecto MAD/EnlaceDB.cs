﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;


namespace Proyecto_MAD
{
    public class EnlaceDB
    {
        static private string _aux { set; get; }
        static private SqlConnection _conexion;
        static private DataTable _tabla = new DataTable();
        static private DataSet _DS = new DataSet();
        static private SqlDataAdapter _adaptador = new SqlDataAdapter();
        static private SqlCommand _comandosql = new SqlCommand();

        public DataTable obtenertabla
        {
            get
            {
                return _tabla;
            }
        }

        private static void conectar()
        {
            //string cnn = ConfigurationManager.AppSettings["desarrollo1"];
            string cnn = ConfigurationManager.ConnectionStrings["desarrollo3"].ToString();
            _conexion = new SqlConnection(cnn);
            _conexion.Open();
        }
        private static void desconectar()
        {
            _conexion.Close();
        }

        public bool Autentificar(string us, string ps)
        {
            bool isValid = false;
            try
            {
                conectar();
                string qry = "SP_ValidaUser";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 9000;

                var parametro1 = _comandosql.Parameters.Add("@u", SqlDbType.Char, 20);
                parametro1.Value = us;
                var parametro2 = _comandosql.Parameters.Add("@p", SqlDbType.Char, 20);
                parametro2.Value = ps;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(_tabla);

                if(_tabla.Rows.Count > 0)
                {
                    isValid = true;
                }

            }
            catch(SqlException e)
            {
                isValid = false;
            }
            finally
            {
                desconectar();
            }

            return isValid;
        }

        public DataTable get_Users()
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "Select Nombre, email, Fecha_modif from Usuarios where Activo = 0;";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.Text;
                _comandosql.CommandTimeout = 1200;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }

        //Departamentos
        public DataTable get_Deptos(string opc, int emp)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Dpto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@empresa", SqlDbType.Int);
                parametro2.Value = emp;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Add_Deptos(string opc, string nombre, int gerente, float sueldo)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Dpto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@nombre_dpto", SqlDbType.VarChar, 20);
                parametro2.Value = nombre;
                var parametro3 = _comandosql.Parameters.Add("@gerente", SqlDbType.VarChar, 20);
                parametro3.Value = gerente;
                var parametro4 = _comandosql.Parameters.Add("@s_base", SqlDbType.VarChar, 20);
                parametro4.Value = sueldo;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);
                

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();                
            }
                
            return table;
        }
        public DataTable update_Deptos(string opc,int id, float sueldo, string nombre, int gerente)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Dpto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;
                

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_dpto", SqlDbType.Int);
                parametro2.Value = id;
                var parametro3= _comandosql.Parameters.Add("@s_base", SqlDbType.Float);
                parametro3.Value = sueldo;
                var parametro4= _comandosql.Parameters.Add("@nombre_dpto", SqlDbType.Char,100);
                parametro4.Value = nombre;
                var parametro5= _comandosql.Parameters.Add("@gerente", SqlDbType.Int);
                parametro5.Value = gerente;
                
                
                    
                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
             }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Deptos(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Dpto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_dpto", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;
        }

        //Puestos
        public DataTable get_Puesto(string opc, int emp)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Puesto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGP", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@empresa", SqlDbType.Int);
                parametro2.Value = emp;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Add_Puesto(string opc, float lvl, string nombre, int dpto)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Puesto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGP", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@lvl_salarial", SqlDbType.Float);
                parametro2.Value = lvl;
                var parametro3 = _comandosql.Parameters.Add("@nombre_puesto", SqlDbType.Char, 100);
                parametro3.Value = nombre;
                var parametro4 = _comandosql.Parameters.Add("@dpto", SqlDbType.Int);
                parametro4.Value = dpto;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;
        }
        public DataTable update_Puesto(string opc, int id, float lvl, string nombre, int dpto)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Puesto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;


                var parametro1 = _comandosql.Parameters.Add("@OpcionGP", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_puesto", SqlDbType.Int);
                parametro2.Value = id;
                var parametro3 = _comandosql.Parameters.Add("@lvl_salarial", SqlDbType.Float);
                parametro3.Value = lvl;
                var parametro4 = _comandosql.Parameters.Add("@nombre_puesto", SqlDbType.Char, 100);
                parametro4.Value = nombre;
                var parametro5 = _comandosql.Parameters.Add("@dpto", SqlDbType.Int);
                parametro5.Value = dpto;



                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Puesto(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Puesto";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGP", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_puesto", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;
        }

        //Empleados
        public DataTable get_Empleado(string opc, int emp)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empleado";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionE", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@empresa", SqlDbType.Int);
                parametro2.Value = emp;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Add_Empleado(string opc, int numero, int domicilio, int banco, int empresa, int puesto, int departamento, 
            string fec_ini_dp, string fec_hired, string fec_puesto) {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empleado";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionE", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@numero", SqlDbType.Int);
                parametro2.Value = numero;
                var parametro3 = _comandosql.Parameters.Add("@domicilio", SqlDbType.Int);
                parametro3.Value = domicilio;
                var parametro4 = _comandosql.Parameters.Add("@banco", SqlDbType.Int);
                parametro4.Value = banco;
                var parametro5 = _comandosql.Parameters.Add("@empresa", SqlDbType.Int);
                parametro5.Value = empresa;
                var parametro6 = _comandosql.Parameters.Add("@puesto", SqlDbType.Int);
                parametro6.Value = puesto;
                var parametro7 = _comandosql.Parameters.Add("@depa", SqlDbType.Int);
                parametro7.Value = departamento;
                var parametro8 = _comandosql.Parameters.Add("@fec_ini_dp", SqlDbType.Date);
                parametro8.Value = fec_ini_dp;
                var parametro9 = _comandosql.Parameters.Add("@fec_hired ", SqlDbType.Date);
                parametro9.Value = fec_hired;
                var parametro10 = _comandosql.Parameters.Add("@fec_puesto", SqlDbType.Date);
                parametro10.Value = fec_puesto;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;
        }
        public DataTable update_Empleado(string opc, int id, int numero, int domicilio, int banco, int empresa, int puesto, int depa,
            string fec_ini_dp, string fec_hired, string fec_puesto)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empleado";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;


                var parametro1 = _comandosql.Parameters.Add("@OpcionE", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@id_empleado", SqlDbType.Int);
                parametro2.Value = id;
                var parametro3 = _comandosql.Parameters.Add("@numero", SqlDbType.Int);
                parametro3.Value = numero;
                var parametro4 = _comandosql.Parameters.Add("@domicilio", SqlDbType.Int);
                parametro4.Value = domicilio;
                var parametro5 = _comandosql.Parameters.Add("@banco", SqlDbType.Int);
                parametro5.Value = banco;
                var parametro6 = _comandosql.Parameters.Add("@empresa", SqlDbType.Int);
                parametro6.Value = empresa;
                var parametro7 = _comandosql.Parameters.Add("@puesto", SqlDbType.Int);
                parametro7.Value = puesto;
                var parametro8 = _comandosql.Parameters.Add("@depa", SqlDbType.Int);
                parametro8.Value = depa;
                var parametro9 = _comandosql.Parameters.Add("@fec_ini_dp", SqlDbType.Date);
                parametro9.Value = fec_ini_dp;
                var parametro10 = _comandosql.Parameters.Add("@fec_hired", SqlDbType.Date);
                parametro10.Value = fec_hired;
                var parametro11 = _comandosql.Parameters.Add("@fec_puesto", SqlDbType.Date);
                parametro11.Value = fec_puesto;



                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }

        public DataTable Delete_Empleado(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empleado";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionE", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@id_empleado", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;



        }

        //falta  update

        //Datos Empleado
        public DataTable get_Datos(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_DatosG";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionDA", SqlDbType.VarChar, 1);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }

        public DataTable Add_Datos(string opc, string nombre, string Nac, string CURP, string NSS, string RFC,
            string email, int tel, string pass)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_DatosG";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionDA", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@nombre", SqlDbType.Char, 100);
                parametro2.Value = nombre;
                var parametro3 = _comandosql.Parameters.Add("@fec_nac", SqlDbType.Date);
                parametro3.Value = Nac;
                var parametro4 = _comandosql.Parameters.Add("@CURP", SqlDbType.Text);
                parametro4.Value = CURP;
                var parametro5 = _comandosql.Parameters.Add("@NSS", SqlDbType.Text);
                parametro5.Value = NSS;
                var parametro6 = _comandosql.Parameters.Add("@RFC", SqlDbType.Text);
                parametro6.Value = RFC;
                var parametro7 = _comandosql.Parameters.Add("@email", SqlDbType.Text);
                parametro7.Value = email;
                var parametro8 = _comandosql.Parameters.Add("@tel", SqlDbType.Int);
                parametro8.Value = tel;
                var parametro9 = _comandosql.Parameters.Add("@pass", SqlDbType.Text);
                parametro9.Value = pass;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable update_Datos(string opc, int id, int tel, string nombre, string fecNac, string curp, string nss, string rfc, string email,
            string pass)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_DatosG";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;


                var parametro1 = _comandosql.Parameters.Add("@OpcionDA", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@id_datos", SqlDbType.Int);
                parametro2.Value = id;
                var parametro3 = _comandosql.Parameters.Add("@nombre", SqlDbType.Char,100);
                parametro3.Value = nombre;
                var parametro4 = _comandosql.Parameters.Add("@fec_nac", SqlDbType.Date);
                parametro4.Value = fecNac;
                var parametro5 = _comandosql.Parameters.Add("@CURP", SqlDbType.Text);
                parametro5.Value = curp;
                var parametro6 = _comandosql.Parameters.Add("@NSS", SqlDbType.Text);
                parametro6.Value = nss;
                var parametro7 = _comandosql.Parameters.Add("@RFC", SqlDbType.Text);
                parametro7.Value = rfc;
                var parametro8 = _comandosql.Parameters.Add("@email", SqlDbType.Text);
                parametro8.Value = email;
                var parametro9 = _comandosql.Parameters.Add("@tel", SqlDbType.Int);
                parametro9.Value = tel;
                var parametro10 = _comandosql.Parameters.Add("@pass", SqlDbType.Text);
                parametro10.Value = pass;



                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Datos(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_DatosG";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionDA", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@id_datos", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;



        }

        //falta update
         
        //Domicilio
        public DataTable get_Dom(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Add_Dom(string opc, int num, string calle, int cp, string colonia, string municipio, string estado)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@calle", SqlDbType.Char,100);
                parametro2.Value = calle;
                var parametro3 = _comandosql.Parameters.Add("@numero", SqlDbType.Int);
                parametro3.Value = num;
                var parametro4 = _comandosql.Parameters.Add("@colonia", SqlDbType.Char, 100);
                parametro4.Value = colonia;
                var parametro5 = _comandosql.Parameters.Add("@municipio", SqlDbType.Char, 100);
                parametro5.Value = municipio;
                var parametro6 = _comandosql.Parameters.Add("@estado", SqlDbType.Char, 100);
                parametro6.Value = estado;
                var parametro7 = _comandosql.Parameters.Add("@CP", SqlDbType.Int);
                parametro7.Value = cp;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;
        }
        public DataTable update_Dom(string opc, int id, string calle, string colonia, int num, string municipio, string estado, int cp)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;


                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@calle", SqlDbType.Char,100);
                parametro2.Value = calle;
                var parametro3 = _comandosql.Parameters.Add("@numero", SqlDbType.Int);
                parametro3.Value = num;
                var parametro4 = _comandosql.Parameters.Add("@colonia", SqlDbType.Char, 100);
                parametro4.Value = colonia;
                var parametro5 = _comandosql.Parameters.Add("@municipio", SqlDbType.Char, 100);
                parametro5.Value = municipio;
                var parametro6 = _comandosql.Parameters.Add("@estado", SqlDbType.Char, 100);
                parametro6.Value = estado;
                var parametro7 = _comandosql.Parameters.Add("@CP", SqlDbType.Int);
                parametro7.Value = cp;
                var parametro8 = _comandosql.Parameters.Add("@num_domicilio", SqlDbType.Int);
                parametro8.Value = id;



                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Dom(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_domicilio", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;



        }

        //falta update

        //Banco
        public DataTable get_Banco(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Banco";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGB", SqlDbType.Char, 1);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Add_Banco(string opc, string nombre, int cuenta)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Banco";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGB", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@nombre", SqlDbType.Char, 24);
                parametro2.Value = nombre;
                var parametro3 = _comandosql.Parameters.Add("@num_cuenta", SqlDbType.Int);
                parametro3.Value = cuenta;
 
                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable update_Banco(string opc, int id, int cuenta, string nombre)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Banco";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;


                var parametro1 = _comandosql.Parameters.Add("@OpcionGB", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_banco", SqlDbType.Int);
                parametro2.Value = id;
                var parametro3 = _comandosql.Parameters.Add("@num_cuenta", SqlDbType.Int);
                parametro3.Value = cuenta;
                var parametro4 = _comandosql.Parameters.Add("@nombre", SqlDbType.Char, 24);
                parametro4.Value = nombre;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Banco(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Banco";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGB", SqlDbType.Char, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_banco", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;



        }

        //Contactos
        public DataTable add_Cts(string opc, string nombre, int tel)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@nombre", SqlDbType.Char, 100);
                parametro2.Value = nombre;
                var parametro3 = _comandosql.Parameters.Add("@tel", SqlDbType.Int);
                parametro3.Value = tel;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Cts(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.Char, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_contactos", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;



        }
        public DataTable update_cts(string opc, int id,string nombre, int tel)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;


                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar,2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_contactos", SqlDbType.Int);
                parametro2.Value = id;
                var parametro3 = _comandosql.Parameters.Add("@nombre", SqlDbType.Char, 100);
                parametro3.Value = nombre;
                var parametro4 = _comandosql.Parameters.Add("@tel", SqlDbType.Int);
                parametro4.Value = tel;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);
            }
            catch (SqlException e)
            {
                //update = false;
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable get_Cts(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }

        //registros
        public DataTable get_Reg(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable add_Reg(string opc, string regf, string regp)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@reg_federal", SqlDbType.Text);
                parametro2.Value = regp;
                var parametro3 = _comandosql.Parameters.Add("@reg_patronal", SqlDbType.Text);
                parametro3.Value = regf;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable update_Reg(string opc, string regf, string regp)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.VarChar, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@reg_federal", SqlDbType.Text);
                parametro2.Value = regp;
                var parametro3 = _comandosql.Parameters.Add("@reg_patronal", SqlDbType.Text);
                parametro3.Value = regf;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Reg(string opc, int id)
        {
            var msg = "";
            DataTable table = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_dir_reg_cont";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGD", SqlDbType.Char, 2);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_registro", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(table);


            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return table;



        }


        //frecuencia
        public DataTable get_Fre(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Frecuencia";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGF", SqlDbType.VarChar, 1);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }


        //Empresa
        public DataTable get_Empresa(string opc)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empresa";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGE", SqlDbType.VarChar, 1);
                parametro1.Value = opc;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Add_Empresa(string opc, string fecop, string rsocial, int reg, int dom, int cts, int fre)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empresa";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGE", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@fec_ini_op", SqlDbType.Date);
                parametro2.Value = fecop;
                var parametro3 = _comandosql.Parameters.Add("@razon_social", SqlDbType.Text);
                parametro3.Value = rsocial;
                var parametro4 = _comandosql.Parameters.Add("@registros", SqlDbType.Int);
                parametro4.Value = reg;
                var parametro5 = _comandosql.Parameters.Add("@domicilio", SqlDbType.Int);
                parametro5.Value = dom;
                var parametro6 = _comandosql.Parameters.Add("@contactos", SqlDbType.Int);
                parametro6.Value = cts;
                var parametro7 = _comandosql.Parameters.Add("@frecuencia", SqlDbType.Int);
                parametro7.Value = fre;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable Delete_Empresa(string opc, int id)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Empresa";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGE", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@num_empresa", SqlDbType.Int);
                parametro2.Value = id;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }



        //Reportes
        public DataTable get_Reporte_General(int anio, int mes, int empresa_id)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "Reporte_General";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@anio", SqlDbType.Int);
                parametro1.Value = anio;

                var parametro2 = _comandosql.Parameters.Add("@mes", SqlDbType.Int);
                parametro2.Value = mes;

                var parametro3 = _comandosql.Parameters.Add("@empresa_id", SqlDbType.Int);
                parametro3.Value = empresa_id;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable get_ReporteHeadcounter1(int anio, int mes, int empresa_id)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "Reporte_Headcounter1";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@anio", SqlDbType.Int);
                parametro1.Value = anio;

                var parametro2 = _comandosql.Parameters.Add("@mes", SqlDbType.Int);
                parametro2.Value = mes;

                var parametro3 = _comandosql.Parameters.Add("@empresa_id", SqlDbType.Int);
                parametro3.Value = empresa_id;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable get_ReporteHeadcounter2(int anio, int mes, int empresa_id)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "Reporte_Headcounter2";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@anio", SqlDbType.Int);
                parametro1.Value = anio;

                var parametro2 = _comandosql.Parameters.Add("@mes", SqlDbType.Int);
                parametro2.Value = mes;

                var parametro3 = _comandosql.Parameters.Add("@empresa_id", SqlDbType.Int);
                parametro3.Value = empresa_id;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable get_Reporte_Nomina(int anio, int mes, int empresa_id)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "Reporte_Nomina";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@anio", SqlDbType.Int);
                parametro1.Value = anio;

                var parametro2 = _comandosql.Parameters.Add("@mes", SqlDbType.Int);
                parametro2.Value = mes;

                var parametro3 = _comandosql.Parameters.Add("@empresa_id", SqlDbType.Int);
                parametro3.Value = empresa_id;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }
        public DataTable get_Resumen_Pagos(int anio)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "Resumen_pagos";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@anio", SqlDbType.Int);
                parametro1.Value = anio;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }


        //Incidencias
        public DataTable Add_Incidencia(string opc, string descripcion, int vacaciones, int deducciones, int percepciones)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Incidencias";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGI", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@vacaciones", SqlDbType.Bit);
                parametro2.Value = vacaciones;
                var parametro3 = _comandosql.Parameters.Add("@descripcion", SqlDbType.Text);
                parametro3.Value = descripcion;
                var parametro4 = _comandosql.Parameters.Add("@deducciones", SqlDbType.Int);
                parametro4.Value = deducciones;
                var parametro5 = _comandosql.Parameters.Add("@percepciones", SqlDbType.Int);
                parametro5.Value = percepciones;

                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }

        //Pagos extra
        public DataTable AddPagos(string opc, float ISR, float p_IMSS)
        {
            var msg = "";
            DataTable tabla = new DataTable();
            try
            {
                conectar();
                string qry = "sp_Gestion_Pagos_ext";
                _comandosql = new SqlCommand(qry, _conexion);
                _comandosql.CommandType = CommandType.StoredProcedure;
                _comandosql.CommandTimeout = 1200;

                var parametro1 = _comandosql.Parameters.Add("@OpcionGPE", SqlDbType.VarChar, 1);
                parametro1.Value = opc;
                var parametro2 = _comandosql.Parameters.Add("@ISR", SqlDbType.Float);
                parametro2.Value = ISR;
                var parametro3 = _comandosql.Parameters.Add("@p_IMSS", SqlDbType.Float);
                parametro3.Value = p_IMSS;


                _adaptador.SelectCommand = _comandosql;
                _adaptador.Fill(tabla);

            }
            catch (SqlException e)
            {
                msg = "Excepción de base de datos: \n";
                msg += e.Message;
                MessageBox.Show(msg, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            finally
            {
                desconectar();
            }

            return tabla;
        }


    }
}
