﻿namespace Proyecto_MAD
{
    partial class Form11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvBanco = new System.Windows.Forms.DataGridView();
            this.Banco = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.NoCuenta = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Next = new System.Windows.Forms.Button();
            this.Accept = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanco)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvBanco
            // 
            this.dgvBanco.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBanco.Location = new System.Drawing.Point(28, 23);
            this.dgvBanco.Name = "dgvBanco";
            this.dgvBanco.RowHeadersWidth = 51;
            this.dgvBanco.RowTemplate.Height = 24;
            this.dgvBanco.Size = new System.Drawing.Size(470, 328);
            this.dgvBanco.TabIndex = 0;
            // 
            // Banco
            // 
            this.Banco.Location = new System.Drawing.Point(542, 116);
            this.Banco.Name = "Banco";
            this.Banco.Size = new System.Drawing.Size(218, 22);
            this.Banco.TabIndex = 36;
            this.Banco.TextChanged += new System.EventHandler(this.Banco_TextChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(539, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 17);
            this.label13.TabIndex = 35;
            this.label13.Text = "Nombre banco";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // NoCuenta
            // 
            this.NoCuenta.Location = new System.Drawing.Point(542, 201);
            this.NoCuenta.Name = "NoCuenta";
            this.NoCuenta.Size = new System.Drawing.Size(218, 22);
            this.NoCuenta.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(539, 181);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 17);
            this.label12.TabIndex = 37;
            this.label12.Text = "No. cuenta";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Next
            // 
            this.Next.Enabled = false;
            this.Next.Location = new System.Drawing.Point(81, 373);
            this.Next.Margin = new System.Windows.Forms.Padding(4);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(89, 31);
            this.Next.TabIndex = 63;
            this.Next.Text = "Volver";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Accept
            // 
            this.Accept.Location = new System.Drawing.Point(671, 373);
            this.Accept.Margin = new System.Windows.Forms.Padding(4);
            this.Accept.Name = "Accept";
            this.Accept.Size = new System.Drawing.Size(89, 31);
            this.Accept.TabIndex = 62;
            this.Accept.Text = "Aceptar";
            this.Accept.UseVisualStyleBackColor = true;
            this.Accept.Click += new System.EventHandler(this.Accept_Click);
            // 
            // Form11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Next);
            this.Controls.Add(this.Accept);
            this.Controls.Add(this.NoCuenta);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Banco);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.dgvBanco);
            this.Name = "Form11";
            this.Text = "Banco";
            this.Load += new System.EventHandler(this.Form11_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvBanco;
        private System.Windows.Forms.TextBox Banco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox NoCuenta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Button Accept;
    }
}