﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Gral : Form
    {
        public Gral()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Gral_Load(object sender, EventArgs e)
        {
                     
             
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int anio = 0, mes = 0, empresa_id = 0;

            anio = int.Parse(textBox1.Text);
            mes = int.Parse(textBox2.Text);
            empresa_id = int.Parse(textBox3.Text);

            EnlaceDB con = new EnlaceDB();
            var tabla3 = con.get_Reporte_General(anio,
                mes,
                empresa_id);
            dgvRG.DataSource = tabla3;
        }
    }
}
