﻿namespace Proyecto_MAD
{
	partial class Form2
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dgvDepto = new System.Windows.Forms.DataGridView();
			this.Modify = new System.Windows.Forms.Button();
			this.Add = new System.Windows.Forms.Button();
			this.Delete = new System.Windows.Forms.Button();
			this.Puesto = new System.Windows.Forms.Button();
			this.Sueldo = new System.Windows.Forms.TextBox();
			this.Nombre = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.menuStrip2 = new System.Windows.Forms.MenuStrip();
			this.departamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.puestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.calcularNominaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.volverEmpresaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.dgvDepto)).BeginInit();
			this.menuStrip2.SuspendLayout();
			this.SuspendLayout();
			// 
			// dgvDepto
			// 
			this.dgvDepto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDepto.Location = new System.Drawing.Point(42, 55);
			this.dgvDepto.Name = "dgvDepto";
			this.dgvDepto.ReadOnly = true;
			this.dgvDepto.RowHeadersWidth = 51;
			this.dgvDepto.Size = new System.Drawing.Size(716, 386);
			this.dgvDepto.TabIndex = 0;
			this.dgvDepto.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDepto_CellContentClick);
			// 
			// Modify
			// 
			this.Modify.Location = new System.Drawing.Point(813, 260);
			this.Modify.Name = "Modify";
			this.Modify.Size = new System.Drawing.Size(130, 25);
			this.Modify.TabIndex = 1;
			this.Modify.Text = "Modificar";
			this.Modify.UseVisualStyleBackColor = true;
			this.Modify.Click += new System.EventHandler(this.Modificar);
			// 
			// Add
			// 
			this.Add.Location = new System.Drawing.Point(813, 195);
			this.Add.Name = "Add";
			this.Add.Size = new System.Drawing.Size(130, 25);
			this.Add.TabIndex = 2;
			this.Add.Text = "Agregar";
			this.Add.UseVisualStyleBackColor = true;
			this.Add.Click += new System.EventHandler(this.Add_Click);
			// 
			// Delete
			// 
			this.Delete.Location = new System.Drawing.Point(813, 330);
			this.Delete.Name = "Delete";
			this.Delete.Size = new System.Drawing.Size(130, 25);
			this.Delete.TabIndex = 3;
			this.Delete.Text = "Borrar";
			this.Delete.UseVisualStyleBackColor = true;
			this.Delete.Click += new System.EventHandler(this.Delete_Click);
			// 
			// Puesto
			// 
			this.Puesto.Location = new System.Drawing.Point(813, 401);
			this.Puesto.Name = "Puesto";
			this.Puesto.Size = new System.Drawing.Size(130, 25);
			this.Puesto.TabIndex = 4;
			this.Puesto.Text = "Volver";
			this.Puesto.UseVisualStyleBackColor = true;
			this.Puesto.Click += new System.EventHandler(this.Puesto_Click);
			// 
			// Sueldo
			// 
			this.Sueldo.Location = new System.Drawing.Point(813, 144);
			this.Sueldo.Name = "Sueldo";
			this.Sueldo.Size = new System.Drawing.Size(130, 20);
			this.Sueldo.TabIndex = 5;
			this.Sueldo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Sueldo_KeyPress);
			// 
			// Nombre
			// 
			this.Nombre.Location = new System.Drawing.Point(813, 92);
			this.Nombre.Name = "Nombre";
			this.Nombre.Size = new System.Drawing.Size(130, 20);
			this.Nombre.TabIndex = 6;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(809, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(65, 20);
			this.label1.TabIndex = 7;
			this.label1.Text = "Nombre";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(809, 122);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(98, 20);
			this.label2.TabIndex = 8;
			this.label2.Text = "Sueldo base";
			// 
			// menuStrip2
			// 
			this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.departamentosToolStripMenuItem,
            this.puestosToolStripMenuItem,
            this.empleadosToolStripMenuItem,
            this.calcularNominaToolStripMenuItem,
            this.volverEmpresaToolStripMenuItem});
			this.menuStrip2.Location = new System.Drawing.Point(0, 0);
			this.menuStrip2.Name = "menuStrip2";
			this.menuStrip2.Size = new System.Drawing.Size(985, 24);
			this.menuStrip2.TabIndex = 10;
			this.menuStrip2.Text = "menuStrip2";
			// 
			// departamentosToolStripMenuItem
			// 
			this.departamentosToolStripMenuItem.Enabled = false;
			this.departamentosToolStripMenuItem.Name = "departamentosToolStripMenuItem";
			this.departamentosToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
			this.departamentosToolStripMenuItem.Text = "Departamentos";
			// 
			// puestosToolStripMenuItem
			// 
			this.puestosToolStripMenuItem.Name = "puestosToolStripMenuItem";
			this.puestosToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
			this.puestosToolStripMenuItem.Text = "Puestos";
			this.puestosToolStripMenuItem.Click += new System.EventHandler(this.puestosToolStripMenuItem_Click);
			// 
			// empleadosToolStripMenuItem
			// 
			this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
			this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
			this.empleadosToolStripMenuItem.Text = "Empleados";
			// 
			// calcularNominaToolStripMenuItem
			// 
			this.calcularNominaToolStripMenuItem.Name = "calcularNominaToolStripMenuItem";
			this.calcularNominaToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
			this.calcularNominaToolStripMenuItem.Text = "Calcular Nomina";
			// 
			// volverEmpresaToolStripMenuItem
			// 
			this.volverEmpresaToolStripMenuItem.Name = "volverEmpresaToolStripMenuItem";
			this.volverEmpresaToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
			this.volverEmpresaToolStripMenuItem.Text = "Volver Empresa";
			this.volverEmpresaToolStripMenuItem.Click += new System.EventHandler(this.volverEmpresaToolStripMenuItem_Click);
			// 
			// Form2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(985, 470);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Nombre);
			this.Controls.Add(this.Sueldo);
			this.Controls.Add(this.Puesto);
			this.Controls.Add(this.Delete);
			this.Controls.Add(this.Add);
			this.Controls.Add(this.Modify);
			this.Controls.Add(this.dgvDepto);
			this.Controls.Add(this.menuStrip2);
			this.MainMenuStrip = this.menuStrip2;
			this.MaximizeBox = false;
			this.Name = "Form2";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Departamentos";
			this.Load += new System.EventHandler(this.Form2_Load);
			((System.ComponentModel.ISupportInitialize)(this.dgvDepto)).EndInit();
			this.menuStrip2.ResumeLayout(false);
			this.menuStrip2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataGridView dgvDepto;
		private System.Windows.Forms.Button Modify;
		private System.Windows.Forms.Button Add;
		private System.Windows.Forms.Button Delete;
		private System.Windows.Forms.Button Puesto;
		private System.Windows.Forms.TextBox Sueldo;
		private System.Windows.Forms.TextBox Nombre;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.MenuStrip menuStrip2;
		private System.Windows.Forms.ToolStripMenuItem departamentosToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem puestosToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem calcularNominaToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem volverEmpresaToolStripMenuItem;
	}
}