﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Form9 : Form
    {
        public Form9()
        {
            InitializeComponent();
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            fechNac.MaxDate = DateTime.Today;
            fechNac.Format = DateTimePickerFormat.Custom;
            fechNac.CustomFormat = "yyyy-MM-dd";

            EnlaceDB con = new EnlaceDB();
            var tabla = con.get_Datos("X");
            dgvData.DataSource = tabla;


            for (int i = 0; i < dgvData.RowCount - 1; i++)
            {
                if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                {
                    fechNac.Text = dgvData.Rows[i].Cells[2].Value.ToString();
                }
            }
        }
        private void Accept_Click(object sender, EventArgs e)
        {
            string nombre=null, curp = null, fec_nac = null, nss = null, rfc = null, email = null, pass = null;
            int tel=0;

            if (Nombre.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount -1; i++) {
                    if ( int.Parse(dgvData.Rows[i].Cells[0].Value.ToString())== aux.id_datos) {
                        nombre = dgvData.Rows[i].Cells[1].Value.ToString();
                     }
                }
            }
            else { nombre = Nombre.Text.ToString(); }
            if (CURP.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount -1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        curp = dgvData.Rows[i].Cells[3].Value.ToString();
                    }
                }
            }
            else { curp = CURP.Text.ToString(); }
            if (fechNac.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount -1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        fec_nac = dgvData.Rows[i].Cells[2].Value.ToString();
                    }
                }
            }
            else { fec_nac = fechNac.Text.ToString(); }
            if (NSS.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount-1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        nss = dgvData.Rows[i].Cells[4].Value.ToString();
                    }
                }
            }
            else { nss = NSS.Text.ToString(); }
            if (RFC.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount-1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        rfc = dgvData.Rows[i].Cells[5].Value.ToString();
                    }
                }
            }
            else { rfc = RFC.Text.ToString(); }
            if (Email.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount-1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        email = dgvData.Rows[i].Cells[6].Value.ToString();
                    }
                }
            }
            else { email = Email.Text.ToString(); }
            if (Contraseña.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount-1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        pass = dgvData.Rows[i].Cells[8].Value.ToString();
                    }
                }
            }
            else { pass = Contraseña.Text.ToString(); }
            if (Tel.Text == "")
            {
                for (int i = 0; i < dgvData.RowCount-1; i++)
                {
                    if (aux.id_datos == int.Parse(dgvData.Rows[i].Cells[0].Value.ToString()))
                    {
                        tel = int.Parse(dgvData.Rows[i].Cells[7].Value.ToString());
                    }
                }
            }
            else{ tel = int.Parse(Tel.Text.ToString());}
            


            EnlaceDB con = new EnlaceDB();
            con.update_Datos("U", aux.id_datos, tel, nombre, fec_nac, curp, nss, rfc, email, pass);

            var tabla = con.get_Datos("X");
            dgvData.DataSource = tabla;
            Accept.Enabled = false;
            Volver.Enabled = true;
            //con.Add_Datos("I",nombre,fec_nac,curp,nss,rfc, fec_nac, email,tel,pass);

            //var tabla = con.get_Datos("X",aux.id_empresa);
            //dgvData.DataSource = tabla;

            //Desactivarlo antes de terminar este perro proyecto
            //Next.Enabled = true;
        }

        private void Next_Click(object sender, EventArgs e)
        {
            //Form10 Dom = new Form10();
            //Dom.Show();
            //this.Hide();
        }

        private void fechNac_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Volver_Click(object sender, EventArgs e)
        {
            Form8 mod = new Form8();
            mod.Show();
            this.Hide();
        }
    }
}
