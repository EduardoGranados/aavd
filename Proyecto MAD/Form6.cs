﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            fecOp.MaxDate = DateTime.Today;
            fecOp.Format = DateTimePickerFormat.Custom;
            fecOp.CustomFormat = "yyyy-MM-dd";

            EnlaceCassandra con = new EnlaceCassandra();
            dgvEmp.DataSource = con.consultaEmpresa();

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            string nombre = null, regf = null, regp = null, calle = null, colonia = null, municipio = null, fecop = null, rsoc = null;
            long tel = 0;

            if (Nombre.Text == "" || R_Fed.Text == "" || R_patr.Text == "" || Calle.Text == "" || Colonia.Text == "" || Municipio.Text == "" ||
                fecOp.Text == "" || R_soc.Text == "" || Tel.Text == "")
            {
                MessageBox.Show("Porfavor llenar todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                nombre = Nombre.Text;
                regf = R_Fed.Text;
                regp = R_patr.Text;
                calle = Calle.Text;
                colonia = Colonia.Text;
                municipio = Municipio.Text;
                fecop = fecOp.Text;
                rsoc = R_soc.Text;
                tel = long.Parse(Tel.Text);

                EnlaceCassandra con = new EnlaceCassandra();
                con.AddEmpresa(Guid.NewGuid(),rsoc,calle,municipio,colonia,fecop,regp,regf,tel,nombre);

                MessageBox.Show("Empresa Agregada correctamente");

                Nombre.Text = "";
                R_Fed.Text = "";
                R_patr.Text = "";
                Calle.Text = "";
                Colonia.Text = "";
                Municipio.Text = "";
                fecOp.Text = "";
                R_soc.Text = "";
                Tel.Text = "";

                dgvEmp.DataSource = con.consultaEmpresa();

                

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            aux.emp = Guid.Parse(dgvEmp.CurrentRow.Cells["id_emp"].Value.ToString());
            Form2 Dpto = new Form2();
            Dpto.Show();
            this.Hide();
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            string nombre = null, regf = null, regp = null, calle = null, colonia = null, municipio = null, fecop = null, rsoc = null;
            long tel = 0;

            if (Nombre.Text == "" || R_Fed.Text == "" || R_patr.Text == "" || Calle.Text == "" || Colonia.Text == "" || Municipio.Text == "" ||
                fecOp.Text == "" || R_soc.Text == "" || Tel.Text == "")
            {
                MessageBox.Show("Porfavor llenar todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                nombre = Nombre.Text;
                regf = R_Fed.Text;
                regp = R_patr.Text;
                calle = Calle.Text;
                colonia = Colonia.Text;
                municipio = Municipio.Text;
                fecop = fecOp.Text;
                rsoc = R_soc.Text;
                tel = long.Parse(Tel.Text);

                EnlaceCassandra con = new EnlaceCassandra();
                con.AddEmpresa(Guid.Parse(dgvEmp.CurrentRow.Cells[0].Value.ToString()), rsoc, calle, municipio, colonia, fecop, regp, regf, tel, nombre);

                MessageBox.Show("Empresa Modificada correctamente");

                dgvEmp.DataSource = con.consultaEmpresa();

                Nombre.Text = "";
                R_Fed.Text = "";
                R_patr.Text = "";
                Calle.Text = "";
                Colonia.Text = "";
                Municipio.Text = "";
                fecOp.Text = "";
                R_soc.Text = "";
                Tel.Text = "";

            }
        }
    


        private void button2_Click(object sender, EventArgs e)
        {
            Gral ReporteGeneral = new Gral();
            ReporteGeneral.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Headcounter2 Reporteheadcounter2 = new Headcounter2(); //puesto
            Reporteheadcounter2.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Reporte_headcounter Reporteheadcounter = new Reporte_headcounter(); //puesto
            Reporteheadcounter.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ReporteNomina ReporteN = new ReporteNomina();
            ReporteN.Show();
        }

        private void Delete_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Estas seguro de eliminar esta empresa?", "Warning!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                EnlaceCassandra con = new EnlaceCassandra();
                con.DeleteEmpresa(Guid.Parse(dgvEmp.CurrentRow.Cells[0].Value.ToString()));


                Nombre.Text = "";
                R_Fed.Text = "";
                R_patr.Text = "";
                Calle.Text = "";
                Colonia.Text = "";
                Municipio.Text = "";
                fecOp.Text = "";
                R_soc.Text = "";
                Tel.Text = "";

                dgvEmp.DataSource = con.consultaEmpresa();
            }



        }

		private void dgvEmp_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex >= 0)
			{
                DataGridViewRow row = this.dgvEmp.Rows[e.RowIndex];

                Nombre.Text = dgvEmp.CurrentRow.Cells["nombre_empresa"].Value.ToString();
                R_Fed.Text = dgvEmp.CurrentRow.Cells["reg_federal"].Value.ToString();
                R_patr.Text = dgvEmp.CurrentRow.Cells["reg_patronal"].Value.ToString();
                Calle.Text = dgvEmp.CurrentRow.Cells["calle"].Value.ToString();
                Colonia.Text = dgvEmp.CurrentRow.Cells["colonia"].Value.ToString();
                Municipio.Text = dgvEmp.CurrentRow.Cells["municipio"].Value.ToString();
                fecOp.Text = dgvEmp.CurrentRow.Cells["fec_ini_op"].Value.ToString();
                R_soc.Text = dgvEmp.CurrentRow.Cells["razon_social"].Value.ToString();
                Tel.Text = dgvEmp.CurrentRow.Cells["tel"].Value.ToString();

            }
		}

		private void Tel_KeyPress(object sender, KeyPressEventArgs e)
		{
            if(e.KeyChar>='a'&&e.KeyChar<='z'|| e.KeyChar >= 'A' && e.KeyChar <= 'Z')
			{
                MessageBox.Show("Solo ingresar números", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                return;
			}
		}

		private void Nombre_KeyPress(object sender, KeyPressEventArgs e)
		{
            if (e.KeyChar >= '0' && e.KeyChar <= '9' )
            {
                MessageBox.Show("Solo ingresar letras", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                return;
            }
        }

		private void Calle_KeyPress(object sender, KeyPressEventArgs e)
		{
            if (e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                MessageBox.Show("Solo ingresar letras", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                return;
            }
        }

		private void Municipio_KeyPress(object sender, KeyPressEventArgs e)
		{
            if (e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                MessageBox.Show("Solo ingresar letras", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                return;
            }
        }
	}
}
