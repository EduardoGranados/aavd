﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
	public partial class Form2 : Form
	{
		public Form2()
		{
			InitializeComponent();
		}



        private void Form2_Load(object sender, EventArgs e)
        {
            EnlaceCassandra con = new EnlaceCassandra();
            dgvDepto.DataSource= con.consultaDepto(aux.emp);

            dgvDepto.Columns[3].Visible = false;
        }

        private void dgvDepto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dgvDepto.Rows[e.RowIndex];

                Nombre.Text = dgvDepto.CurrentRow.Cells["nombre_dpto"].Value.ToString();
                Sueldo.Text = dgvDepto.CurrentRow.Cells["s_base"].Value.ToString();

            }
        }
        private void Add_Click(object sender, EventArgs e)
        {
            string nombre = null;
            float sueldo = 0;

            if (Sueldo.Text == ""||
            Nombre.Text == "")
			{
                MessageBox.Show("Tiene que llenar todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
                nombre = Nombre.Text;
                sueldo = float.Parse(Sueldo.Text);

                EnlaceCassandra con = new EnlaceCassandra();
                con.AddDepto(Guid.NewGuid(), nombre, sueldo, aux.emp);

                MessageBox.Show("Departamento agregado correctamente");

                dgvDepto.DataSource = con.consultaDepto(aux.emp);
                dgvDepto.Columns[3].Visible = false;

                Sueldo.Text = "";
                Nombre.Text = "";
            }


           // Gerente.Text = "";
        }

        private void Modificar(object sender, EventArgs e)
        {
            string nombre = null;
            float sueldo = 0;

            if (Sueldo.Text == "" ||
            Nombre.Text == "")
            {
                MessageBox.Show("Tiene que llenar todos los campos", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                nombre = Nombre.Text;
                sueldo = float.Parse(Sueldo.Text);

                EnlaceCassandra con = new EnlaceCassandra();
                con.AddDepto(Guid.Parse(dgvDepto.CurrentRow.Cells["id_dpto"].Value.ToString()), nombre, sueldo, aux.emp);

                MessageBox.Show("Departamento modificado correctamente");

                dgvDepto.DataSource = con.consultaDepto(aux.emp);
                dgvDepto.Columns[3].Visible = false;


                Sueldo.Text = "";
                Nombre.Text = "";
            }
        }

        private void Puesto_Click(object sender, EventArgs e)
        {
            Form6 Back = new Form6();
            Back.Show();
            this.Hide();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro de eliminar este departamento?", "Warning!", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                EnlaceCassandra con = new EnlaceCassandra();
                con.DeleteDpto(Guid.Parse(dgvDepto.CurrentRow.Cells["id_dpto"].Value.ToString()));



                Sueldo.Text = "";
                Nombre.Text = "";

                dgvDepto.DataSource = con.consultaDepto(aux.emp);
                dgvDepto.Columns[3].Visible = false;

            }
           // Gerente.Text = "";
        }

		private void Sueldo_KeyPress(object sender, KeyPressEventArgs e)
		{
            if (e.KeyChar >= 'a' && e.KeyChar <= 'z' || e.KeyChar >= 'A' && e.KeyChar <= 'Z')
            {
                MessageBox.Show("Solo ingresar números", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                return;
            }
        }

		private void puestosToolStripMenuItem_Click(object sender, EventArgs e)
		{
            Form4 Puesto = new Form4();
            Puesto.Show();
            this.Hide();
        }

		private void volverEmpresaToolStripMenuItem_Click(object sender, EventArgs e)
		{
            Form6 Emp = new Form6();
            Emp.Show();
            this.Hide();
		}
	}
}
