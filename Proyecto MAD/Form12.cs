﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_MAD
{
    public partial class Form12 : Form
    {
        public Form12()
        {
            InitializeComponent();
        }

        private void Form12_Load(object sender, EventArgs e)
        {
            DateDpto.MaxDate = DateTime.Today;
            DateDpto.Format = DateTimePickerFormat.Custom;
            DateDpto.CustomFormat = "yyyy-MM-dd";

            Datepuesto.MaxDate = DateTime.Today;
            Datepuesto.Format = DateTimePickerFormat.Custom;
            Datepuesto.CustomFormat = "yyyy-MM-dd";

            FecHired.MaxDate = DateTime.Today;
            FecHired.Format = DateTimePickerFormat.Custom;
            FecHired.CustomFormat = "yyyy-MM-dd";

            EnlaceDB con = new EnlaceDB();
            var tabla3 = con.get_Deptos("X", aux.id_empresa);
            dgvDepto.DataSource = tabla3;


            var tabla4 = con.get_Puesto("X",aux.id_empresa);
            dgvPuestos.DataSource = tabla4;

            var tabla6 = con.get_Empleado("X", aux.id_empresa);
            dgvEmp.DataSource = tabla6;

            for (int i = 0; i < dgvEmp.RowCount - 1; i++)
            {
                if (aux.id_emp == int.Parse(dgvEmp.Rows[i].Cells[0].Value.ToString()))
                {
                    FecHired.Text = dgvEmp.Rows[i].Cells[10].Value.ToString();
                    DateDpto.Text = dgvEmp.Rows[i].Cells[8].Value.ToString();
                    Datepuesto.Text = dgvEmp.Rows[i].Cells[11].Value.ToString();
                }
            }




            try
            {
                dgvPuestos.Columns[4].Visible = false;
                dgvPuestos.Columns[5].Visible = false;
                dgvPuestos.Columns[6].Visible = false;
                dgvPuestos.Columns[7].Visible = false;
                dgvPuestos.Columns[8].Visible = false;
                dgvPuestos.Columns[9].Visible = false;
                dgvPuestos.Columns[10].Visible = false;
                dgvPuestos.Columns[11].Visible = false;
                dgvPuestos.Columns[12].Visible = false;
                dgvPuestos.Columns[13].Visible = false;
                dgvPuestos.Columns[14].Visible = false;
                dgvPuestos.Columns[15].Visible = false;
                dgvPuestos.Columns[16].Visible = false;
                dgvPuestos.Columns[17].Visible = false;
                dgvPuestos.Columns[18].Visible = false;
            }
            catch (InvalidCastException) { }

            var tabla5 = con.get_Deptos("X",aux.id_empresa);
            dgvDepto.DataSource = tabla5;


            try
            {
                dgvDepto.Columns[4].Visible = false;
                dgvDepto.Columns[5].Visible = false;
                dgvDepto.Columns[6].Visible = false;
                dgvDepto.Columns[7].Visible = false;
                dgvDepto.Columns[8].Visible = false;
                dgvDepto.Columns[9].Visible = false;
                dgvDepto.Columns[10].Visible = false;
                dgvDepto.Columns[11].Visible = false;
                dgvDepto.Columns[12].Visible = false;
                dgvDepto.Columns[13].Visible = false;
                dgvDepto.Columns[14].Visible = false;
            }
            catch (InvalidCastException) { }
        }

        private void DateDpto_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgvBanco_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Accept_Click(object sender, EventArgs e)
        {
            string fecp = null, fecd = null, fech = null;
            int depto = 0, puesto = 0;

            if (Departamento.Text == "")
            {
                for (int i = 0; i < dgvEmp.RowCount -1; i++)
                {
                    if (aux.id_emp == int.Parse(dgvEmp.Rows[i].Cells[0].Value.ToString()))
                    {
                        depto = int.Parse(dgvEmp.Rows[i].Cells[7].Value.ToString());
                    }
                }
            }
            else { depto = int.Parse(Departamento.Text); }
            if (Puesto.Text == "")
            {
                for (int i = 0; i < dgvEmp.RowCount -1; i++)
                {
                    if (aux.id_emp == int.Parse(dgvEmp.Rows[i].Cells[0].Value.ToString()))
                    {
                        puesto = int.Parse(dgvEmp.Rows[i].Cells[6].Value.ToString());
                    }
                }
            }
            else { puesto = int.Parse(Puesto.Text); }
            fecp = Datepuesto.Text;
            fecd = DateDpto.Text;
            fech = FecHired.Text;

            EnlaceDB con = new EnlaceDB();
            con.update_Empleado("U", aux.id_emp, aux.id_datos, aux.id_dom, aux.id_banco, aux.id_empresa, puesto, depto, fecd, fech, fecp);

            Accept.Enabled = false;
            Next.Enabled = true;

        }

        private void dgvDepto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Next_Click(object sender, EventArgs e)
        {
            Form8 mod = new Form8();
            mod.Show();
            this.Hide();
        }
    }
}
